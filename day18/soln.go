// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 18.

package main

import (
	"aoc"
	"fmt"
)

// Turn this on to get execution tracing.
const trace = false

// Thread states.
type tstate int
const (
	// Thread is ready to run.
	tsRunning = tstate(iota)
	// Thread is blocked on rcv.
	tsBlocked
	// Thread has exited.
	tsExited
)

// Machine per-thread state.
type state struct {
	// Thread ID.
	tid int
	// Machine registers.
	regs map[reg]int
	// Program Counter.
	pc int
	// Liveness of the thread.
	// XXX Flag is a terrible name.
	flag tstate
	// Number of times the thread has executed snd.
	sndCount int
}

// Create a new thread state, initializing things
// appropriately.
func makeState(tid int) state {
	// Default values are mostly fine.
	var pstate state
	// Need the register map to exist.
	pstate.regs = make(map[reg]int)
	// Must save the thread ID.
	pstate.tid = tid
	if tid >= 0 {
		pstate.regs[reg(int('p' - 'a'))] = tid
	}
	return pstate
}

// Run instructions on the given thread state until it
// blocks or exits.
func run(prog []insn, st *state) {
	for st.flag == tsRunning {
		// Show execution trace if desired.
		if trace {
			fmt.Println(*st)
		}
		// Can exit by walking off end.
		if st.pc < 0 || st.pc >= len(prog) {
			st.flag = tsExited
		} else {
			prog[st.pc].exec(st)
		}
	}
}

// Solve the problems.
func main() {
	part1 := aoc.Setup()
	prog := assemble()
	if part1 {
		// Make and run a single thread with ID -1.
		st := makeState(-1)
		run(prog, &st)
		// Show the resulting message.
		switch st.flag {
		case tsExited:
			n := len(messages[0])
			if n == 0 {
				panic("receive before send")
			}
			fmt.Println("rcv", messages[0][0])
			return
		default:
			panic("internal error: blocked in part 1")
		}
	} else {
		// Make and run two threads (with ID 0 and
		// 1) to completion.
		threads := []state{makeState(0), makeState(1)}
		working := true
		for working {
			// Run each thread that is willing.
			for i := 0; i < 2; i++ {
				if threads[i].flag == tsRunning {
					run(prog, &threads[i])
				}
			}
			// Unblock threads that have snds
			// available.
			working = false
			for i := 0; i < 2; i++ {
				if threads[i].flag == tsBlocked &&
					len(messages[i]) > 0 {
					threads[i].flag = tsRunning
				}
				if threads[i].flag == tsRunning {
					working = true
				}
			}
		}
		// Show the answer.
		fmt.Println(threads[1].sndCount)
	}
}
