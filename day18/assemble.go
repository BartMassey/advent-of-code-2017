// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 18 instruction assembler.

package main

import (
	"aoc"
	"strings"
)

// Try to interpret the source string as a register name.
// If it is not a register name, return impossible register
// -1 and false.
func parseReg(source string) (reg, bool) {
	// Turn the string into its chars.
	chars := []rune(source)
	// Check that there is one character in the Unicode
	// lowercase alphabet.
	if len(chars) != 1 || chars[0] < 'a' || chars[0] > 'z' {
		// Nope. Fail.
		return reg(-1), false
	}
	// Return the register with the number corresponding
	// to the name.
	return reg(chars[0] - 'a'), true
}

// Try to interpret the source string as a register name or
// literal constant. Panic with a dubious error message if
// neither works.
func parseRc(source string) rc {
	// Try register first.
	r, is_reg := parseReg(source)
	if is_reg {
		return r
	}
	// Try constant.
	return cnst(aoc.Atoi(source))
}

// Interpret source as a single instruction and try to
// assemble it.
func assembleInsn(source string) insn {
	// Break the instruction into fields.
	fields := strings.Fields(source)
	if len(fields) == 0 {
		// Blank lines are currently not
		// allowed. Bleah.
		panic("blank line")
	}

	// Panic if the number of operands does not match
	// the instruction.
	checkl := func(n int) {
		if len(fields) != n + 1 {
			panic("wrong number of operands")
		}
	}

	// Panic if the given source operand s is not a
	// register name. Otherwise return its register.
	parse_r := func(s string) reg {
		r, ok := parseReg(s)
		if !ok {
			panic("bad register name")
		}
		return r
	}

	// Assemble the opcode and its operands.  There
	// isn't much to say here.
	switch fields[0] {
	case "snd":
		checkl(1)
		op := parseRc(fields[1])
		i := iSnd{op}
		return &i
	case "set":
		checkl(2)
		op1 := parse_r(fields[1])
		op2 := parseRc(fields[2])
		i := iSet{op1, op2}
		return &i
	case "add":
		checkl(2)
		function := binopAdd{}
		op1 := parse_r(fields[1])
		op2 := parseRc(fields[2])
		i := iBinop{function, op1, op2}
		return &i
	case "mul":
		checkl(2)
		function := binopMul{}
		op1 := parse_r(fields[1])
		op2 := parseRc(fields[2])
		i := iBinop{function, op1, op2}
		return &i
	case "mod":
		checkl(2)
		function := binopMod{}
		op1 := parse_r(fields[1])
		op2 := parseRc(fields[2])
		i := iBinop{function, op1, op2}
		return &i
	case "rcv":
		checkl(1)
		op := parseRc(fields[1])
		i := iRcv{op}
		return &i
	case "jgz":
		checkl(2)
		op1 := parseRc(fields[1])
		op2 := parseRc(fields[2])
		i := iJgz{op1, op2}
		return &i
	}
	panic("unknown instruction")
}

// Read and assemble a source program from standard input.
func assemble() []insn {
	// Assembled instructions.
	var result []insn
	// Read and assemble each source instruction.
	for aoc.ScanLine() {
		istr := aoc.NextLine()
		i := assembleInsn(istr)
		result = append(result, i)
	}
	// Check for read errors and return assembled
	// instructions.
	aoc.LineErr()
	return result
}
