# Advent of Code 2017: Day 18
Copyright (c) 2017 Bart Massey

Well that was a lot of tedious typing and fiddly
debugging. My final solution is more than 300 lines of Go.
I split it into multiple files for
manageability. Unfortunately, Go's support for this isn't
perfect: `go run soln.go` doesn't work anymore.

There was a lot of detail in both parts of this
specification. I missed a bunch of little things even with
careful reading.

Go's interface-based pseudo-object system worked mostly OK
for this. I ended up needing a type-switch at one point,
which is disappointing. I used some empty structs to get
the method dispatch to work right, which is also
disappointing. On the upside, the static checker caught a
bunch of things I screwed up, and the runtime checks also
triggered usefully a few times.

I ended up submitting wrong answers to Part 2 twice. All in
all, this was not a fun day.

Set `GOPATH` as per top-level README. Run with

    go run *.go 1 <input.txt
    go run *.go 2 <input.txt

or

    go build
    ./day18 1 <input.txt
    ./day18 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
