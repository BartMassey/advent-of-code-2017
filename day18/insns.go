// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 18 instruction descriptions.

package main

// A machine register.
type reg int

// A machine constant value.
type cnst int

// Value that can be represented by a register or a
// constant.
type rc interface {
	// Figure out a value for the rc.
	eval(st *state) int
}

// Evaluate a register.
func (r reg) eval(st *state) int {
	return st.regs[r]
}

// Evaluate a constant.
func (i cnst) eval(st *state) int {
	return int(i)
}

// An instruction may be executed in a given state.
type insn interface {
	exec(st *state)
}

// Message queue for snd/rcv.
var messages [2][]int

// Snd instruction. Is it "sound" or "send"?
type iSnd struct {
	// Source to snd.
	snd rc
}

// Execute snd.
func (i *iSnd) exec(st *state) {
	// Get the operand.
	s := i.snd.eval(st)
	if st.tid < 0 {
		// Part 1: just store the value at front-of-queue.
		messages[0] = []int{s}
	} else {
		// Part 2: enqueue the value.
		messages[1-st.tid] = append(messages[1-st.tid], s)
	}
	// Count number of sends for Part 2.
	st.sndCount++
	// Set up for next instruction.
	st.pc++
}

// Set instruction.
type iSet struct {
	// Target of set.
	dest reg
	// Value to set.
	source rc
}

// Execute set.
func (i *iSet) exec(st *state) {
	// Update the register with the value.
	st.regs[i.dest] = i.source.eval(st)
	// Set up for next instruction.
	st.pc++
}

// An binop can compute its value from its two arguments.
type binop interface {
	compute(int, int) int
}

// Add operator. Requires no state.
type binopAdd struct {}

// Implement add operator.
func (_ binopAdd) compute(x, y int) int {
	return x + y
}

// Mul operator. Requires no state.
type binopMul struct {}

// Implement mul operator.
func (_ binopMul) compute(x, y int) int {
	return x * y
}

// Mod operator. Requires no state.
type binopMod struct {}

// Implement mod operator.
func (_ binopMod) compute(x, y int) int {
	return x % y
}

// A binop instruction (add, mul mod).
type iBinop struct {
	// Operator to use.
	function binop
	// Left-hand operand and target for result.
	dest reg
	// Right-hand operand.
	source rc
}

// Implement binop.
func (i *iBinop) exec(st *state) {
	// Get the LHS.
	x := i.dest.eval(st)
	// Get the RHS.
	y := i.source.eval(st)
	// Compute and store the result.
	st.regs[i.dest] = i.function.compute(x, y)
	// Set up for next instruction.
	st.pc++
}

// Rcv instruction. Is it "recover" or "receive"?
type iRcv struct {
	// Rcv target register.
	rcv rc
}

// Execute rcv instruction.
func (i *iRcv) exec(st *state) {
	if st.tid < 0 {
		// Part 1. Use register or constant as condition.
		x := i.rcv.eval(st)
		if x == 0 {
			// If condition is zero skip to next instruction.
			st.pc++
		} else {
			// If condition is nonzero stop the computation.
			st.flag = tsExited
		}
	} else {
		// Part 2. Register is receive target.
		if len(messages[st.tid]) == 0 {
			// If the queue is empty we will have to wait.
			// Note that we will restart this instruction
			// when run again.
			st.flag = tsBlocked
			return
		} else {
			// Queue has a message. Must store into a reg.
			switch r := i.rcv.(type) {
			case cnst:
				panic("part 2 rcv dest constant")
			case reg:
				// Put the message in the register.
				st.regs[r] = messages[st.tid][0]
				// Dequeue the message.
				messages[st.tid] = messages[st.tid][1:]
				// Set up for next instruction.
				st.pc++
			}
		}
	}
}

// Jgz instruction.
type iJgz struct {
	// Jump condition.
	cond rc
	// Jump relative offset.
	offset rc
}

// Execute jgz instruction.
func (i *iJgz) exec(st *state) {
	// Get and check the condition.
	x := i.cond.eval(st)
	if x > 0 {
		// Bump the program counter by the specified
		// amount.
		st.pc += i.offset.eval(st)
	} else {
		// No jump. Set up for next instruction.
		st.pc++
	}
}
