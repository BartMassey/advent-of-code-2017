# Advent of Code 2017: Day 3
Copyright (c) 2017 Bart Massey

More of a fiddly math exercise than a test of Go
programming, but I learned some things about the language.
Go has no built-in integer `abs()`, for example (?).

I solved Part 1 with a bunch of terribly tedious modulo
calculations that I got wrong about a billion times. For
Part 2, I finally just hit the Internet and grabbed
someone's fancy code that computes Cartesian Coordinates for
points on an Ulam Spiral.

I have since rewritten both parts (with a few hours of
effort) based on a from-scratch implementation of the Spiral
Coordinates function. This made the code quite a bit
simpler. Turns out Go has no built-in integer square root or
even `max()`, so I got an external package rather than
rolling my own.

Set `GOPATH` as per top-level README, then

    go get github.com/cznic/mathutil

Run with

    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt


I later added `soln.py` because I wanted bignums for a
question I was answering and Go's solution looked too
painful.

    python3 soln.py 1 <input.txt
    python3 soln.py 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
