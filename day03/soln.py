#!/usr/bin/python3
# Copyright © 2017 Bart Massey
# This program is licensed under the "MIT License".
# Please see the file COPYING in this distribution
# for license terms.

# Advent of Code Day 3 Part 2.

import sys

# The missing integer square root function.
def isqrt(n):
    i = 0
    while (i + 1) * (i + 1) <= n:
        i += 1
    return i

# Given a spiral coordinate n, return the corresponding
# Cartesion coordinate, in a system where the first spiral
# coordinate is 1 at (0, 0).
def spiralCoord(n):
    # Sadly, 1 is a special case.
    if n == 1:
        return (0, 0)
    # Ring number, counting outward from 1.
    nRing = (isqrt(n - 1) + 3) // 2
    # Sqrt of largest value in next ring in.
    maxPrevRingSqrt = 2 * (nRing - 1) - 1
    # Position along the ring of our coordinate.
    base = n - maxPrevRingSqrt**2
    # Length of a side of the current ring.
    side = 2 * nRing - 1
    # Which of four contiguous segments of the current
    # ring n lies in.
    seg = (base - 1) // (side - 1)
    # Position of n within its segment.
    offset = base - seg * (side - 1)
    # This is enough information to compute the
    # Cartesian coordinates in a reasonably uniform
    # way.
    if seg ==  0:
        return (nRing - 1, offset - nRing + 1)
    elif seg == 1:
        return (nRing - offset - 1, nRing - 1)
    elif seg == 2:
        return (-nRing + 1, nRing - offset - 1)
    elif seg == 3:
        return (offset - nRing + 1, -nRing + 1)
    else:
        assert True, "altSpiralCoord: bad seg"

# Solution to Part 1. Just walk in on both x and y until at
# the center.
def soln1(n):
	x, y = spiralCoord(n)
	return abs(x) + abs(y)

# Essentially a Fibonacci-style dynamic
# programming calculation but with 2-3 neighbors.
def soln2(n):
    # Keep a map of values we've computed so far.
    spiral = dict()
    # Start the recurrence with the initial value.
    spiral[spiralCoord(1)] = 1
    # Now run the recurrence forward until the input is
    # exceeded, then take the last answer.
    i = 2
    while True:
        # Where are we?
        x, y = spiralCoord(i)
        # Compute the recurrence.
        t = 0
        for dx in range(-1, 2):
            for dy in range(-1, 2):
                dc = (x + dx, y + dy)
                if dc in spiral:
                    t += spiral[dc]
        # Is this what we were looking for?
        if t > n:
            return t
        # Save for future computation.
        spiral[(x, y)] = t
        # Next recurrence step.
        i += 1

n = int(input())
if len(sys.argv) == 1 or sys.argv[1] == "1":
    print(soln1(n))
elif sys.argv[1] == "2":
    print(soln2(n))
else:
    assert False, "bad argument"
