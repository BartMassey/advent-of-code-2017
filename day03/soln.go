// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 3.

package main

import (
	"aoc"
	"fmt"
)

// The mysteriously-missing absolute value function.
func abs(n int) int {
	if n >= 0 {
		return n
	}
	return -n
}

// A 2D grid point.
type Point struct {
	x, y int
}

// Given a spiral coordinate n, return the corresponding
// Cartesion Coordinate, in a system where the first spiral
// coordinate is 1 at (0, 0).
func spiralCoord(n int) Point {
	// Sadly, 1 is a special case.
	if n == 1 {
		return Point{0, 0}
	}
	// Ring number, counting outward from 1.
	nRing := (aoc.ISqrt(n - 1) + 3) / 2
	// Sqrt of largest value in next ring in.
	maxPrevRingSqrt := 2 * (nRing - 1) - 1
	// Position along the ring of our coordinate.  Go
	// offers no way to do integer exponentiation.
	base := n - maxPrevRingSqrt * maxPrevRingSqrt
	// Length of a side of the current ring.
	side := 2 * nRing - 1
	// Which of four contiguous segments of the current
	// ring n lies in.
	seg := (base - 1) / (side - 1)
	// Position of n within its segment.
	offset := base - seg * (side - 1)
	// This is enough information to compute the
	// Cartesian coordinates in a reasonably uniform
	// way.
	switch seg {
	case 0:
		return Point{nRing - 1, offset - nRing + 1}
	case 1:
		return Point{nRing - offset - 1, nRing - 1}
	case 2:
		return Point{-nRing + 1, nRing - offset - 1}
	case 3:
		return Point{offset - nRing + 1, -nRing + 1}
	default:
		panic("altSpiralCoord: bad seg")
	}
}

// Solution to Part 1. Just walk in on both x and y until at
// the center.
func soln1(n int) int {
	p := spiralCoord(n)
	return abs(p.x) + abs(p.y)
}

// Solution to Part 2. Essentially a Fibonacci-style dynamic
// programming calculation but with 2-3 neighbors.
func soln2(n int) int {
	// Keep a map of values we've computed so far.
	spiral := map[Point] int {}
	// Start the recurrence with the initial value.
	spiral[spiralCoord(1)] = 1
	// Now run the recurrence forward until the input is
	// exceeded, then take the last answer.
	i := 2
	for {
		// Where are we?
		c := spiralCoord(i)
		// Compute the recurrence.
		t := 0
		for dx := -1; dx <= 1; dx++ {
			for dy := -1; dy <= 1; dy++ {
				dc := Point{c.x + dx, c.y + dy}
				if elem, ok := spiral[dc]; ok {
					t += elem
				}
			}
		}
		// Is this what we were looking for?
		if t > n {
			return t
		}
		// Save for future computation.
		spiral[c] = t
		// Next recurrence step.
		i++
	}
}

// Solve the problem.
func main() {
	// Set up and process input.
	part1 := aoc.Setup()
	n := aoc.Atoi(aoc.ReadLine())
	// Solve desired part.
	if part1 {
		fmt.Println(soln1(n))
	} else {
		fmt.Println(soln2(n))
	}
}
