// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 11.

// The hex grid stuff used here is from
// http://www.redblobgames.com/grids/hexagons

package main

import (
	"aoc"
	"fmt"
	"strings"
)

// Hex grid "cube coordinate". Invariant: x + y + z = 0
type Coord struct {
	x, y, z int
}

// Interface for manipulating cube coordinates.
type Coordinator interface {
	move(dirn string)
	oDist() int
	inc(*Coord)
}

// Return the offset of each coordinate for a move in the
// given named direction.
func delta(dirn string) Coord {
	switch dirn {
	case "n":
		return Coord{ 0, 1, -1 }
	case "s":
		return Coord{ 0, -1, 1 }
	case "nw":
		return Coord{ -1, 1, 0 }
	case "se":
		return Coord{ 1, -1, 0 }
	case "ne":
		return Coord{ 1, 0, -1 }
	case "sw":
		return Coord{ -1, 0, 1 }
	default:
		panic("bad direction")
	}
}

// Adjust each coordinate of loc by d.
func (loc *Coord) inc(d *Coord) {
	loc.x += d.x
	loc.y += d.y
	loc.z += d.z
}


// Adjust loc to reflect a move in direction dirn.
func (loc *Coord) move(dirn string) {
	d := delta(dirn)
	loc.inc(&d)
}

// Integer absolute value.
func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

// "Hex taxicab" distance from loc to the origin.
func (loc *Coord) oDist() int {
	return (abs(loc.x) + abs(loc.y) + abs(loc.z)) / 2
}

func main() {
	// Set up and get input.
	part1 := aoc.Setup()
	line := aoc.ReadLine()
	steps := strings.Split(line, ",")
	// Current position.
	loc := Coord{ 0, 0, 0 }
	// Maximum distance from origin seen so far.
	max_dist := 0
	// Take each step.
	for _, s := range steps {
		loc.move(s)
		// Adjust maximum distance.
		dist := loc.oDist()
		if dist > max_dist {
			max_dist = dist
		}
	}
	if part1 {
		fmt.Println(loc.oDist())
	} else {
		fmt.Println(max_dist)
	}
}
