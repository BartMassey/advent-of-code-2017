# Advent of Code 2017: Day 11
Copyright (c) 2017 Bart Massey

Having worked with hex grids before, this was a matter of
finding the most awesome Red Blob Games
[Hex Grid webpage](https://www.redblobgames.com/grids/hexagons/#coordinates)
and implementing the direction step and origin distance for
cube coordinates.

I can't believe that Go doesn't have an integer `max` or
`abs`. So terrible.

This problem gave a nice opportunity to use Go's pseudo-OO
interface types in a trivial way.

To run the Part 1 tests, set `GOPATH` as per top-level README and
run with

    sh run-tests.sh

The two numbers on each output line should match.

To get the puzzle answers, set `GOPATH` as per top-level
README and run with

    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
