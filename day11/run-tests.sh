#!/bin/sh
# Copyright © 2017 Bart Massey
# This program is licensed under the "MIT License".
# Please see the file COPYING in this distribution
# for license terms.

# Run tests for Part 1 of current day.

while read TEST RESULT
do
    echo -n "$RESULT "
    echo "$TEST" | go run soln.go 1
done <tests1.txt
