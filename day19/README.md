# Advent of Code 2017: Day 19
Copyright (c) 2017 Bart Massey

Straightforward state machine exercise. I got rank 247 for
second star on the leaderboard, my best performance so
far. I seem to be getting more familiar with Go.

Go's lack of tuples really hurts in problems like this: also
the missing `min`, `max` and `abs` functions. These things
are real conveniences when working with integer grids. There
is a lot of (careful) copy-paste coding here that should be
cleaned up.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
