// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 19.

package main

import (
	"aoc"
	"fmt"
)

func main() {
	part1 := aoc.Setup()
	// Set up the character grid.
	var grid [][]rune
	for aoc.ScanLine() {
		row := aoc.NextLine()
		chars := []rune(row)
		grid = append(grid, chars)
	}
	aoc.LineErr()
	// Find the starting column.
	r := 0
	c := 0
	for grid[r][c] != '|' {
		c++
	}
	// Start moving down.
	dr := 1
	dc := 0
	// Letters seen.
	var seen []rune
	// Whether to take another step.
	walking := true
	// Number of steps taken.
	steps := 0
	// Start walking.
	for walking {
		// Took another step.
		steps++
		// Now in a new location.
		r = r + dr
		c = c + dc
		// What to do depends on what's here.
		switch grid[r][c] {
		case '|', '-':
			// Doesn't matter which direction we
			// are going: just keep going in
			// that direction.
		case ' ':
			// Just walked off the end. Stop.
			walking = false
		case '+':
			// At a corner. Try to turn.
			if dr == 0 {
				// Going across. Now go up
				// or down.
				dc = 0
				if grid[r + 1][c + dc] != ' ' {
					// Can go down.
					dr = 1
				} else if grid[r - 1][c + dc] != ' ' {
					// Can go up.
					dr = -1
				} else {
					panic("trapped horizontally")
				}
			} else if dc == 0 {
				// Going down. Now go left
				// or right.
				dr = 0
				if grid[r + dr][c + 1] != ' ' {
					// Can go right.
					dc = 1
				} else if grid[r + dr][c - 1] != ' ' {
					// Can go left.
					dc = -1
				} else {
					panic("trapped vertically")
				}
			} else {
				panic("weird direction")
			}
		default:
			// Hit a letter. Remember it and
			// keep going.
			letter := grid[r][c]
			if letter < 'A' || letter > 'Z' {
				panic("mystery symbol")
			}
			seen = append(seen, letter)
		}
	}
	if part1 {
		// Show letters seen.
		fmt.Println(string(seen))
	} else {
		// Show number of steps.
		fmt.Println(steps)
	}
}
