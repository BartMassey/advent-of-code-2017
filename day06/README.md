# Advent of Code 2017: Day 6
Copyright (c) 2017 Bart Massey

Oh Go! This is a quite straightforward problem, except we
desperately want a set or map data structure that accepts a
sequence of ints as its key type. Go doesn't do this.

Fortunately (?) we can serialize the sequence of ints into a
string and use that as the key. Neat.

Code was relatively straightfoward except for working this
out. Not a proud moment for our friend Go.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
