// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 6.

package main

import (
	"aoc"
	"fmt"
	"strings"
	"unicode"
)

func main() {
	// Set up machinery, read and parse input.
	part1 := aoc.Setup()
	line := aoc.ReadLine()
	heap_descs := strings.FieldsFunc(line, unicode.IsSpace)
	var heaps []int
	for _, v := range heap_descs {
		hi := aoc.Atoi(v)
		heaps = append(heaps, hi)
	}
	// Set of previously-seen heaps. This is indexed by
	// the string serialization of the heaps, since Go
	// is super-limited in what types can be used as map
	// keys.
	history := map[string] bool {}
	// Serialized version of current heap for indexing.
	hi := fmt.Sprint(heaps)
	// For Part 2, whether we are skipping to the start
	// or trying to find a loop.
	looping := false
	for {
		// Insert seen heap into set.
		history[hi] = true
		// Find the maximum value and location in the heaps.
		v_max := heaps[0]
		i_max := 0
		for i, v := range heaps {
			if v > v_max {
				v_max = v
				i_max = i
			}
		}
		// Do the distribution.
		h := heaps[i_max]
		heaps[i_max] = 0
		j := (i_max + 1) % len(heaps)
		for i := 0; i < h; i++ {
			heaps[j]++
			j = (j + 1) % len(heaps)
		}
		// Deal with end cases.
		hi = fmt.Sprint(heaps)
		if history[hi] {
			if part1 || looping {
				// For Part 1, first match works.
				// For Part 2, second match works.
				fmt.Println(len(history))
				return
			}
			// Finished Part 2 runout. Reset
			// state for looping cycle.
			history = map[string]bool {}
			looping = true
		}
	}
	panic("No cycle found")
}
