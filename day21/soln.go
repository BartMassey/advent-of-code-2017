// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 21.

package main

import (
	"aoc"
	"fmt"
	"os"
	"strings"
)

// Type of images, really.
type pat [][]bool

// Convert a string representation into a pat.
func readPat(desc string) pat {
	rows := strings.Split(desc, "/")
	n := len(rows)
	pattern := make([][]bool, n)
	for i, v := range rows {
		if len(v) != n {
			panic("bad pat")
		}
		cs := []rune(v)
		for _, c := range cs {
			switch c {
			case '.':
				pattern[i] = append(pattern[i], false)
			case '#':
				pattern[i] = append(pattern[i], true)
			default:
				panic("bad pat char")
			}
		}
	}
	return pattern
}

// A rule has a LHS pattern and a RHS substitution.
type rule [2]pat

// Read the rules from stdin.
func readRules() []rule {
	var rules []rule
	for aoc.ScanLine() {
		desc := aoc.NextLine()
		sides := strings.Split(desc, " => ")
		if len(sides) != 2 {
			panic("bad rule")
		}
		r := rule{readPat(sides[0]), readPat(sides[1])}
		rules = append(rules, r)
	}
	return rules
}

// Display a pat for debugging purposes.
func printPat(p pat) {
	for _, r := range p {
		for _, v := range r {
			if v {
				fmt.Printf("#")
			} else {
				fmt.Printf(".")
			}
		}
		fmt.Println()
	}
}

// Make a fresh pat of the given dimension.
func makePat(n int) pat {
	result := make([][]bool, n)
	for i := range result {
		result[i] = make([]bool, n)
	}
	return result
}

// Make a fresh copy of a pat for debugging purposes.  Not
// actually needed.
func copyPat(target pat) pat {
	n := len(target)
	result := make([][]bool, n)
	for i, r := range target {
		result[i] = make([]bool, n)
		for j, v := range r {
			result[i][j] = v
		}
	}
	return result
}

// Return a counter-clockwise 90° rotated copy of pat.
func rotatePat(p pat) pat {
	n := len(p)
	result := makePat(n)
	for i, r := range p {
		for j, v := range r {
			result[n - j - 1][i] = v
		}
	}
	return result
}

// Return a horizontally reflected copy of pat.
func reflectPat(p pat) pat {
	n := len(p)
	result := makePat(n)
	for i, r := range p {
		for j, v := range r {
			result[i][n - j - 1] = v
		}
	}
	return result
}

// Return true iff two pats match. Could replace with
// reflect.DeepEquals() if desired.
func eqPat(p1, p2 pat) bool {
	if len(p1) != len(p2) || len(p1[0]) != len(p2[0]) {
		return false
	}
	for i, r := range p1 {
		for j, v := range r {
			if v != p2[i][j] {
				return false
			}
		}
	}
	return true
}

// Return all rotations and/or reflections of given patern.
func transformsPat(p pat) []pat {
	var ts []pat
	// Make all eight possible rotations and reflections
	// of the pattern. Note that we make the input one
	// last (double reflection, quadruple rotation) to
	// make sure we are getting copies.
	for i := 0; i < 2; i++ {
		p = reflectPat(p)
		for j := 0; j < 4; j++ {
			p = rotatePat(p)
			ts = append(ts, p)
		}
	}
	return ts
}

// Type of compiled matcher. XXX this is pretty specialized.
type matcher struct {
	m2 map[[2][2]bool] pat
	m3 map[[3][3]bool] pat
}

// Return hashmap for the given ruleset with all the pattern
// transformations applied.
func compileRules(rs []rule) matcher {
	var trs matcher
	trs.m2 = make(map[[2][2]bool]pat)
	trs.m3 = make(map[[3][3]bool]pat)
	for i := range rs {
		ts := transformsPat(rs[i][0])
		for _, v := range ts {
			switch len(v) {
			case 2:
				if len(v[0]) != 2 {
					panic("unexpected 2 pat")
				}
				var p [2][2] bool
				for i := 0; i < 2; i++ {
					for j := 0; j < 2; j++ {
						p[i][j] = v[i][j]
					}
				}
				trs.m2[p] = rs[i][1]
			case 3:
				if len(v[0]) != 3 {
					panic("unexpected 3 pat")
				}
				var p [3][3] bool
				for i := 0; i < 3; i++ {
					for j := 0; j < 3; j++ {
						p[i][j] = v[i][j]
					}
				}
				trs.m3[p] = rs[i][1]
			default:
				panic("unexpected pat size")
			}
		}
	}
	return trs
}

// Copy out a subpattern of dimension n with upper-left
// corner (r, c) from target and return it.
func extractPat(target pat, r, c, n int) pat {
	result := makePat(n)
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			result[i][j] = target[r + i][c + j]
		}
	}
	return result
}

// Copy a source pattern into target starting at
// upper-left corner (r, c).
func applyPat(target, source pat, r, c int) {
	n := len(source)
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			target[r + i][c + j] = source[i][j]
		}
	}
}

// Return the number of "on" (true) pixels in pat.
func countPat(target pat) int {
	n := len(target)
	count := 0
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			if target[i][j] {
				count++
			}
		}
	}
	return count
}

// Try to match the LHS of some rule using the matcher.
// When a rule matches, return the RHS. If no rule matches,
// just crash.
func applyRules(m matcher, target pat) pat {
	switch len(target) {
	case 2:
		var t [2][2]bool
		for i := 0; i < 2; i++ {
			for j := 0; j < 2; j++ {
				t[i][j] = target[i][j]
			}
		}
		p, ok := m.m2[t]
		if !ok {
			panic("bad 2 match")
		}
		return p
	case 3:
		var t [3][3]bool
		for i := 0; i < 3; i++ {
			for j := 0; j < 3; j++ {
				t[i][j] = target[i][j]
			}
		}
		p, ok := m.m3[t]
		if !ok {
			panic("bad 3 match")
		}
		return p
	}
	panic("internal error: bad rule length")
}

// Solve the problem.
func main() {
	part1 := aoc.Setup()
	// Vary the number of iterations. For the test
	// problem, give iteration count as an argument.
	var iters int
	if len(os.Args) > 2 {
		iters = aoc.Atoi(os.Args[2])
	} else if part1 {
		iters = 5
	} else {
		iters = 18
	}
	// Read in the rules.
	rules := readRules()
	// Compile matcher out of rules.
	mx := compileRules(rules)
	// The starting pattern is fixed.
	target := readPat(".#./..#/###")
	// Run the specified number of iterations.
	for itr := 0; itr < iters; itr++ {
		// Find the modulus and number of blocks of
		// the input.
		n := len(target)
		var q int
		if n % 2 == 0 {
			q = 2
		} else {
			if n % 3 != 0 {
				panic("bad q")
			}
			q = 3
		}
		b := n / q
		// Set up the output.
		subst := makePat(b * (q + 1))
		// Do the substitution at every block in the
		// target.
		for i := 0; i < b; i++ {
			for j := 0; j < b; j++ {
				// Get the block pattern out.
				p := extractPat(target, i * q, j * q, q)
				// Find the substitution.
				x := applyRules(mx, p)
				// Make the substitution in the output.
				applyPat(subst, x, i * (q + 1), j * (q + 1))
			}
		}
		// Old output is new target.
		target = subst
	}
	fmt.Println(countPat(target))
}
