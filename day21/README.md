# Advent of Code 2017: Day 21
Copyright (c) 2017 Bart Massey

Sigh. So much tedious typing and debugging. 200+ lines of
just banging pixels around.

Go doesn't help, since it has no built-in deep copy, no way
to initialize arrays of arrays in one go, no reasonable deep
equals (theres a thing in the `reflect` module, but uggh),
no way to iterate `1..n` short of C-style. Most importantly,
no maps with slice keys. This already bit me once, but I
don't want to think about the speed penalty of converting
back and forth from strings here.

Went back and optimized everything. First pass was to move
the pattern transformations into the ruleset instead of the
targets. This got me from about 9s to about 750ms. The next
step was a horrible gross Go mess with separate hash-tables
for the patterns of dimension 2 and 3 and copying back and
forth from slices. This got me to 150ms.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
