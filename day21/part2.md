# Day 21: Part Two

*How many pixels stay on* after `18` iterations?

Your puzzle answer was `2758764`.
