// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 1.

package main

import (
	"aoc"
	"fmt"
)

// Print the solution to a given part in the obvious
// brute-force fashion.
func main() {
	// Get the input.
	part1 := aoc.Setup()
	seq := aoc.ReadLine()
	// Grab the input length.
	n := len(seq)
	// Set up the accumulator.
	sum := 0
	// Walk the sequence.
	for i := 0; i < n; i++ {
		// Get the comparison target. Modulo
		// arithmetic is used for wraparound.
		var j uint
		if part1 {
			j = uint((i + 1) % n)
		} else {
			j = uint((i + n/2) % n)
		}
		// Bump accumulator if appropriate.
		if seq[i] == seq[j] {
			sum += int(seq[i] - '0')
		}
	}
	// Report the answer.
	fmt.Printf("%d\n", sum)
}
