# Advent of Code 2017: Day 1
Copyright (c) 2017 Bart Massey

This was a quite straightforward warmup exercise. Forced me
to look up how to read stdin in Go, how to do some basic
string handling, and how to get command-line arguments.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

A C++ solution to Part 2 has been included for comparison.

    g++ -O4 -Wall -o soln2 soln2.cpp
    ./soln2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
