// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 1.

#include <iostream>
using namespace std;

// Print the solution to a given part in the obvious
// brute-force fashion.
int main() {
    string seq;
    cin >> seq;
    // Grab the input length.
    int n = seq.length();
    // Set up the accumulator.
    int sum = 0;
    // Walk the sequence.
    for (int i = 0; i < n; i++) {
        // Get the comparison target. Modulo
        // arithmetic is used for wraparound.
        int j = (i + n / 2) % n;
        // Bump accumulator if appropriate.
        if (seq[i] == seq[j]) {
            sum += seq[i] - '0';
	}
    }
    // Report the answer.
    cout << sum << endl;
}
