// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code setup library.

package aoc

import (
	"bufio"
	"log"
	"os"
	"strconv"
)

// Current line number for reader.
var nLine int = 1
// Scanner for reader.
var scanner *bufio.Scanner = bufio.NewScanner(os.Stdin)

// Set up for an Advent of Code problem. Processes the
// command-line.  Returns true if the user asked for Part 1,
// and false for Part 2.
func Setup() bool {
	// By default, assume Part 1 for convenience.
	part1 := true
	// If there is a part arg, choose given Part.
	if len(os.Args) >= 2 {
		switch os.Args[1] {
		case "1":
			part1 = true
		case "2":
			part1 = false
		default:
			log.Fatalf("unknown part %s", os.Args[1])
		}
	}
	return part1
}

// Print a message and exit if the reader has encountered an
// error.
func LineErr() {
	if err := scanner.Err(); err != nil {
		log.Fatalf("line %d: error reading line from stdin: %v",
			nLine, err)
	}
}

// Returns true iff there is another line available to be
// read.
func ScanLine() bool {
	return scanner.Scan()
}

// Reads and returns the next line. No checking is
// performed. Suitable for iterating with a for loop.
func NextLine() string {
	nLine += 1
	return scanner.Text()
}

// Reads and returns the next line. Checks for availability
// and errors.
func ReadLine() string {
	if !scanner.Scan() {
		LineErr()
		log.Fatalf("line %d: tried to read line past end of stdin",
			nLine)
	}
	return NextLine()
}

// Parse an integer and panic if it fails.
func Atoi(s string) int {
	val, err := strconv.Atoi(s)
	if err != nil {
		panic(err)
	}
	return val
}
