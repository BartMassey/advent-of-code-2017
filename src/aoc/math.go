// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code setup library: math functions.

package aoc

import "github.com/cznic/mathutil"

func Min(x, y int) int {
	if x <= y {
		return x
	} else {
		return y
	}
}

func Max(x, y int) int {
	if x >= y {
		return x
	} else {
		return y
	}
}

func Abs(x int) int {
	if x >= 0 {
		return x
	} else {
		return -x
	}
}

// The missing integer square root function.  XXX 32-bit
// only.
func ISqrt(n int) int {
	return int(mathutil.ISqrt(uint32(n)))
}
