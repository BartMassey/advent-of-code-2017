// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// The "Knot Hash" (see Advent of Code Day 10).

package knothash

// In-place reverse a subsequence of the circular buffer
// ring starting at posn and with length length.
func circularReverse(ring []int, posn int, length int) {
	// Get the modulus for wrap.
	n := len(ring)
	// Swap start position within the reversal.
	i := 0
	// Loop until the indices meet/cross, which
	// will mean that everything has been reversed.
	for {
		// Swap end position within the reversal.
		j := length - 1 - i
		// If the end has met/crossed the start,
		// get out.
		if j <= i {
			break
		}
		// Swap start and end position within the
		// ring.
		pi := (posn + i) % n
		pj := (posn + j) % n
		// Make the swap.
		ring[pi], ring[pj] = ring[pj], ring[pi]
		// Advance the position.
		i++
	}
}

// "Shuffle the ring" as the first step of a Knot Hash with
// the given parameters. If salt_key is true, extra
// characters will be appended to the key. Return the
// shuffled ring.
func ShuffleRing(ring_size, rounds int, salt_key bool, key string) []int {
	// Treat the key as ASCII (Unicode) characters.
	// Convert string to chars.
	length_chars := []rune(key)
	// Build the lengths array as specified.
	var lengths []int
	for _, v := range length_chars {
		lengths = append(lengths, int(v))
	}
	if salt_key {
		// "Extra" characters to stick on the end.
		extras := []int {17, 31, 73, 47, 23}
		for _, v := range extras {
			lengths = append(lengths, v)
		}
	}
	// Set up the ring as specified.
	ring := make([]int, ring_size)
	for i := range ring {
		ring[i] = i
	}
	// Set up round state and run the rounds.
	posn := 0
	skip := 0
	for i := 0; i < rounds; i++ {
		// Run a round.
		for _, length := range lengths {
			// Do the reversal.
			circularReverse(ring, posn, length)
			// Advance the position.
			posn = (posn + length + skip) % ring_size
			// Increment the skip.
			skip++
		}
	}
	return ring
}

// Compute the 16-byte "Knot Hash" of an input string.
func KnotHash(key string) [16]uint8 {
	ring := ShuffleRing(256, 64, true, key)
	// Work on 16-element blocks.
	b := 16
	if b * b != len(ring) {
		panic("internal error: bad block size")
	}
	// Checksum for each block.
	var result [16]uint8
	// Block index.
	bi := 0
	// Extract checkums.
	for block := 0; block < len(ring); block += b {
		// Can start with 0 since x ^ 0 == x.
		h := 0
		// Xor in all the characters.
		for i := 0; i < b; i++ {
			h = h ^ ring[block + i]
		}
		// Get the digits.
		result[bi] = uint8(h)
		bi++
	}
	return result
}
