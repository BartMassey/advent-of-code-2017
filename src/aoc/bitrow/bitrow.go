// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// A "BitRow" is an infinite bit vector indexed by a signed
// value. This implementation is designed to be "fast"
// compared to map[int]bool .

package bitrow

// BitRow, including cache.
type BitRow struct {
	// Backing store is a blocked map.
	bits map[int]int64
	// Cache validity flag.
	last_valid bool
	// Cache block id, if valid.
	last_blockid int
	// Cache block contents, if valid.
	last_block int64
}

// Make a new BitRow.
func New() BitRow {
	return BitRow{
		make(map[int]int64),
		false,
		0,
		0,
	}
}

// Get checks the writeback cache for the value.
func (br *BitRow) Get(i int) bool {
	// Get bit coordinates.
	blockid := i / 64
	if i < 0 {
		blockid--
	}
	bitpos := uint(i) % 64
	// Flush the cache if needed.
	if br.last_valid && blockid != br.last_blockid {
		br.bits[br.last_blockid] = br.last_block
		br.last_valid = false
	}
	// Shadow of eventual block value.
	var block int64
	// Fill the cache if needed.
	if br.last_valid {
		// Block is in cache.
		block = br.last_block
	} else {
		// Fill the cache.
		block = br.bits[blockid]
		br.last_valid = true
		br.last_blockid = blockid
		br.last_block = block
	}
	b := ((block >> bitpos) & 1) == 1
	return b
}

// Set uses the writeback cache for the value.
func (br *BitRow) Set(i int, b bool) {
	// Get bit coordinates.
	blockid := i / 64
	if i < 0 {
		blockid--
	}
	bitpos := uint(i) % 64
	// Flush the cache if needed.
	if br.last_valid && blockid != br.last_blockid {
		br.bits[br.last_blockid] = br.last_block
		br.last_valid = false
	}
	// Shadow of eventual block value.
	var block int64
	// Fill the cache if needed.
	if br.last_valid {
		// Block is in cache.
		block = br.last_block
	} else {
		// Missed the cache.
		block = br.bits[blockid]
	}
	// Modify the block.
	if b {
		block |= 1 << bitpos
	} else {
		block &= ^(1 << bitpos)
	}
	// Cache the block.
	br.last_valid = true
	br.last_blockid = blockid
	br.last_block = block
}

// Make a bool map for convenience.
func (br *BitRow) ToMap() map[int]bool {
	// Write-through the cache if needed.
	if br.last_valid {
		br.bits[br.last_blockid] = br.last_block
	}
	result := make(map[int]bool)
	for k, v := range br.bits {
		base := k * 64
		for i := 0; i < 64; i++ {
			if ((v >> uint(i)) & 1) == 1 {
				result[base + int(uint(i))] = true
			}
		}
	}
	return result
}
