# Advent of Code 2017: Day 7
Copyright (c) 2017 Bart Massey

Go is really starting to kind of tick me off.

So today was the first problem with fancy syntax for its
input. I duly set about to use Go's `regexp` package to
parse it, as I had done in previous years with Rust and
Haskell.

After a couple of hours, here's a thing I learned. If I use
Go's engine to match the regular expression

    (([a-z]+) )*([a-z]+)

against the string

    hello relevant world

I will get a match vector back that looks like

    ["hello relevant world", "relevant", "world"]

Hello? As far as I can tell, there's no way to get all the
matches of the Kleene star. There's some obvious
workarounds, one of which I used, but…ouch?

For Part 2 I wanted to use a recursive algorithm that
returned either the weight of the given tree if balanced or
the fail-weight for correction if unbalanced (to propagate
to the top). Disjoint union would be really nice here, but
Go doesn't have that. There's a hacky workaround involving
interface subtypes and type switches, but honestly ugh. I
could have just returned the negation of a fail-weight in
this instance, which doesn't sound error-prone at all. I
chose to print the answer and exit the program early when
unbalance was found.

I misread Part 2 and forgot to try the test case, so I
submitted a wrong answer. Noted and corrected.

The input is apparently conditioned quite carefully to avoid
ambiguous cases in Part 2. A single child can never have the
wrong weight by definition. With two children, it is
impossible to tell which is wrong on a disagreement. With
three or more, all but one have to agree. I implemented all
of this, but I won't necessarily notice bugs in the input
because early exit.

This was quite a lot of reasonably fancy code. Feels like
we've kicked it up a notch. I forgot to save the code I
actually submitted with. It was a little less pretty, but
not substantially different.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
