// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 7.

package main

import (
	"aoc"
	"fmt"
	"os"
	"regexp"
)

// The basic structure of the input line.
var rowRE *regexp.Regexp =
	regexp.MustCompile(`^([a-z]+) \(([0-9]+)\)( -> (([a-z]+, )*([a-z]+)))?$`)
// Used to split the neighbors.
var neighborRE *regexp.Regexp =
	regexp.MustCompile(", ")

// Weight of each node.
var weights map[string]int = map[string]int {}
// List of neighbors (children) for each node. Nodes with no
// children have an empty list.
var neighbors map[string][]string = map[string][]string {}

// Pick up the instance data and stuff it in the globals.
func readInstance() {
	// Read and process input lines.
	for aoc.ScanLine() {
		line := aoc.NextLine()
		// Get top-level parse.
		pats := rowRE.FindStringSubmatch(line)
		if pats == nil {
			panic("syntax error")
		}
		// Get mandatory fields.
		thisNode := pats[1]
		thisWeight := aoc.Atoi(pats[2])
		// Get children if present.
		neighbors[thisNode] = []string{}
		weights[thisNode] = thisWeight
		if pats[4] != "" {
			neighbors[thisNode] = neighborRE.Split(pats[4], -1)
		}
	}
	// Check for read errors.
	aoc.LineErr()
}

// Find the root of the implicit tree by enumeration.
func soln1() string {
	// A node is dominated if there is another on top of it.
	dominated := map[string]bool {}
	for key, _ := range weights {
		dominated[key] = false
	}
	for _, val := range neighbors {
		for _, subval := range val {
			dominated[subval] = true
		}
	}
	// The top node is the unique undominated node.
	for key, val := range dominated {
		if !val {
			return key
		}
	}
	panic("no root found")
}

// Solve Part 2 by recursive search. Returns the weight of
// the tree at this node (if it doesn't exit).
func soln2(node string) int {
	// Take a look at the children.
	children := neighbors[node]
	// Base case: "top of tower".
	if len(children) == 0 {
		return weights[node]
	}
	// Weight of each child tree.
	var child_weights []int
	// Histogram of child tree weights.
	weight_hist := map[int]int {}
	// Accumulated weight of child trees.
	total_weight := 0
	// Walk over the children collecting state.
	for _, c := range children {
		// Recursive case.
		w := soln2(c)
		// Gather up the state information.
		child_weights = append(child_weights, w)
		total_weight += w
		weight_hist[w] += 1
	}
	// Found the mismatch.
	if len(weight_hist) > 1 {
		if len(weight_hist) != 2 {
			panic("too many weights")
		}
		// Find out what the right and wrong
		// weights are.
		var match_weight int
		var mismatch_weight int
		for w, n := range weight_hist {
			if n == len(child_weights) - 1 {
				match_weight = w
			} else if n == 1 {
				mismatch_weight = w
			} else {
				panic("unexpected hist weight")
			}
		}
		// Find the index of the wrong weight, print
		// the correction and exit.
		for i, w := range child_weights {
			if w == mismatch_weight {
				// Weight of all children of bad child.
				grandchildren_weight :=
					w - weights[children[i]]
				// Adjust bad child weight for correct total.
				correct_weight :=
					match_weight - grandchildren_weight
				fmt.Println(correct_weight)
				os.Exit(0)
			}
		}
	}
	// Our total weight is our children's weight plus
	// our own.
	return weights[node] + total_weight
}

// Run the requested solution.
func main() {
	// Get ready to solve.
	part1 := aoc.Setup()
	readInstance()
	// Need the root for both parts.
	top := soln1()
	if part1 {
		// Already done.
		fmt.Println(top)
	} else {
		// This search will not return if successful.
		soln2(top)
		panic("no solution found")
	}
}
