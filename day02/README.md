# Advent of Code 2017: Day 2
Copyright (c) 2017 Bart Massey

This was another fairly straightforward warmup exercise.
Can get a tiny bit of performance by sorting the values from
largest to smallest in part 2.

I wrote a bunch of extra code building a nice 2D data
structure with all the rows in it for part 1 in anticipation
of part 2.  Processing a row at a time works fine for both
parts: never anticipate part 2.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
