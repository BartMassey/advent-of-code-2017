// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 2.


package main

import (
	"aoc"
	"fmt"
	"strings"
	"unicode"
	"sort"
)

// Read the "spreadsheet" from stdin as a 2D slice
// structure.
func readSheet() [][]int {
	// Set up the sheet.
	var sheet [][]int
	// Read lines of input into the sheet.
	for aoc.ScanLine() {
		// Read and split an input line.
		line := aoc.NextLine()
		row := strings.FieldsFunc(line, unicode.IsSpace)
		// Process the line into integers.
		var r []int
		for _, sj := range row {
			ij := aoc.Atoi(sj)
			r = append(r, ij)
		}
		// Stick the integers into the sheet.
		sheet = append(sheet, r)
	}
	// Check for read errors, then return sheet.
	aoc.LineErr()
	return sheet
}

// Walk rows of the sheet to compute and print a sheet
// checksum.
func soln1(sheet [][]int) {
	// Set up checksum.
	sum := 0
	// Walk over rows of the sheet.
	for _, r := range sheet {
		// The min (max) is not smaller (larger) than
		// the first value.
		small := r[0]
		large := r[0]
		// Improve the min and max over the whole row.
		for _, v := range r {
			if v < small {
				small = v
			}
			if v > large {
				large = v
			}
		}
		// Add the difference to the sum.
		sum += large - small
	}
	// Show the final sum.
	fmt.Println(sum)
}


// Walk rows of the sheet to compute and print a sheet
// checksum.
func soln2(sheet [][]int) {

	// Returns some quotient of two elements e1 and e2 of
	// row r such that e2 | e1, if possible.
	FindQ := func (r []int) int {
		// Smaller numbers divide larger numbers.
		sort.Sort(sort.Reverse(sort.IntSlice(r)))
		// Look for and return a candidate if possible.
		for i := range r {
			for j := i + 1; j < len(r); j++ {
				if r[i] % r[j] == 0 {
					return r[i] / r[j]
				}
			}
		}
		panic("no q in row")
	}

	// Set up and compute the sum over the rows.
	sum := 0
	for _, r := range sheet {
		sum += FindQ(r)
	}
	// Display the checksum.
	fmt.Println(sum)
}

// Solve the problem.
func main() {
	// Set up machinery.
	part1 := aoc.Setup()
	// Get the input.
	sheet := readSheet()
	// Process the input.
	if part1 {
		soln1(sheet)
	} else {
		soln2(sheet)
	}
}
