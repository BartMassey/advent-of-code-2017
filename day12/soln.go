// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 12.

package main

import (
	"aoc"
	"fmt"
	"regexp"
)

// The basic structure of the input line.
var rowRE *regexp.Regexp =
	regexp.MustCompile(`^([0-9]+) <-> (([0-9]+, )*([0-9]+))$`)
// Used to split the neighbors.
var neighborRE *regexp.Regexp =
	regexp.MustCompile(", ")

// Read an instance from stdin and return it. The result
// will be an adjacency list of int vertices.
func readInstance() map[int][]int {
	// Set up the result list.
	var neighbors map[int][]int = map[int][]int {}
	// Read and process input lines.
	for aoc.ScanLine() {
		line := aoc.NextLine()
		// Get top-level parse.
		pats := rowRE.FindStringSubmatch(line)
		if pats == nil {
			panic("syntax error")
		}
		// Get source.
		thisNode := aoc.Atoi(pats[1])
		// Get and save adjacencies.
		var nv []int
		for _, vs := range neighborRE.Split(pats[2], -1) {
			nv = append(nv, aoc.Atoi(vs))
		}
		neighbors[thisNode] = nv
	}
	return neighbors
}

// A partition entry.
type Partition int

// A partition table.
type UnionFind []Partition

// Create a new partition table with n entries.
func newUnionFind(n int) UnionFind {
	var parent []Partition
	for i := 0; i < n; i++ {
		parent = append(parent, Partition(i))
	}
	return parent
}

// Find the canonical representative of the given partition.
func (this UnionFind) find(i Partition) Partition {
	// Walk up the tree to the canonical representative.
	p := i
	for this[p] != p {
		p = this[p]
	}
	// Shorten all the pointers up the tree for efficiency.
	for this[i] != p {
		t := this[i]
		this[i] = p
		i = t
	}
	return p
}

// Join the partitions containing i1 and i2.
func (this UnionFind) union(i1, i2 Partition) {
	p1 := this.find(i1)
	p2 := this.find(i2)
	this[p2] = p1
}

// Solve the problem.
func main() {
	// Get the problem setup.
	part1 := aoc.Setup()
	neighbors := readInstance()
	// Build the partition table.
	var uf UnionFind = newUnionFind(len(neighbors))
	for v1, vs := range neighbors {
		for _, v2 := range vs {
			uf.union(Partition(v1), Partition(v2))
		}
	}
	// Find the number of elements in partition 0, and
	// the set of partition canonical representatives.
	// Start by finding the root of partition 0.
	p0 := uf.find(0)
	// Number of elements known to be in partition 0.
	np0 := 0
	// Set of partition canonical representatives.
	var heads map[int]bool = map[int]bool {}
	// Walk the union-find table accumulating answers.
	for i, _ := range uf {
		// Find the canonical representative for this element.
		p := uf.find(Partition(i))
		// Add that CR to the set of CRs.
		heads[int(p)] = true
		// If it's CR(0), bump the count.
		if p == p0 {
			np0++
		}
	}
	if part1 {
		fmt.Println(np0)
	} else {
		// Cardinality of set.
		fmt.Println(len(heads))
	}
}
