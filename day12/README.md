# Advent of Code 2017: Day 12
Copyright (c) 2017 Bart Massey

Chose to solve this one with union-find. Not sure it was the
best plan, but it worked fine. Certainly blindingly fast.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
