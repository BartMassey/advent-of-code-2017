// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day <day>.

package main

import (
	"aoc"
	"fmt"
)

func main() {
	aoc.Setup()
}
