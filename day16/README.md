# Advent of Code 2017: Day 16
Copyright (c) 2017 Bart Massey

Huh. Part 1 was pretty straightforward. Just do a little
permutation dance like we've done in AoC before.

Part 2 was a bit challenging. Seemed obvious that even with
a really fast interpreter we couldn't run 10^9 iterations.

A little Googling pointed out that one can raise a
permutation to a power by just going to that power mod the
order of the permutation. After a good amount of time
working on this approach, I finally realized that because of
the exchange-by-label it wasn't the same permutation
everytime.

From there it was a short step to realizing that I could
maybe find a short cycle that came back to the starting
permutation.  I suppose the cycle needn't have been back to
start: would cross that bridge when I came to it.  For my
input, there was a cycle of length 60, and the residue 10^9
mod 60 = 40. So the 40th of the 60 steps in my cycle was
the correct answer.

Reddit pointed out that I needn't have gotten "lucky": using
fast exponentiation to do a smaller number *n* of iterations
guarantees success.  In my case, it would have been better
than break-even, since

> *n = ceiling(log2(10^9)) + popcount(10^9) = 30 + 13 = 43*

Of course I couldn't leave well enough alone, and came back
later to optimize the compiler. Now the 1000 input
instructions are replaced with 31 or fewer (26 in practice)
equivalent instructions by tracking the permutations and
spins and generating each permutation as pairwise swaps and
(maybe) a single spin at the end.

So our executed instruction count is 26 * 60 = 1560. A
couple of orders of magnitude out of pencil-and-paper range,
but still not bad. Runs in about 3ms on my box.

Would have been nice to have some kind of full test for Part
2.

Learned about Go's `reflect.DeepEqual()`, which is maybe a
good thing? Learned some permutation tricks.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
