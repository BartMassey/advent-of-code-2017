// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 16.

package main

import (
	"aoc"
	"container/list"
	"fmt"
	"os"
	"reflect"
	"strings"
)

// Turn on to show permutation trace.
const tracePermute bool = false

// "programs" (in the problem description sense): a new type
// for type safety.
type prog int

// Spin instruction.
type insnSpin struct {
	// Distance to spin.
	count int
}

// Exchange instruction.
type insnEXchange struct {
	// Indices to exchange.
	i, j int
}

// Partner instruction.
type insnPartner struct {
	// Partners to swap.
	a, b prog
}

// Generic instruction interface.
type insn interface {
	// Execute an instruction.
	exec(posns []prog)
}


// Rotate the positions as described in the problem description.
func (s *insnSpin) exec(posns []prog) {
	n := len(posns)
	// Make a copy of the input. There is a copy-free
	// plan if one is careful, I think, but it's hard
	// to find.
	tmp := make([]prog, n)
	copy(tmp, posns)
	// Fill each element in the input slice from the
	// appropriate position in the temporary slice.
	for i := range tmp {
		j := (i + n - s.count) % n
		posns[i] = tmp[j]
	}
}

// Exchange the positions as described in the problem description.
func (x *insnEXchange) exec(posns []prog) {
	posns[x.i], posns[x.j] = posns[x.j], posns[x.i]
}

// Return the index of target in positions. Nothing fancy,
// takes time O(|posns|).
func find(posns []prog, target prog) int {
	for i, v := range posns {
		if v == target {
			return i
		}
	}
	panic("not found")
}

// Swap the partners as described in the problem description.
func (p *insnPartner) exec(posns []prog) {
	i := find(posns, p.a)
	j := find(posns, p.b)
	posns[i], posns[j] = posns[j], posns[i]
}

// Take the positions and return a ready-to-print string.
func permString(posns []prog) string {
	result := ""
	for _, p := range posns {
		result += string(rune(int('a') + int(p)))
	}
	return result
}

// Identity set of positions (the start in the problem
// description).
func permId(order int) []prog {
	posns := make([]prog, order)
	for i := range posns {
		posns[i] = prog(i)
	}
	return posns
}

// Exchange-type instruction functionality.
type xgen interface {
	// One-character name of exchange instruction.
	name() string
	// Generate an exchange instruction for given positions.
	gen(i1, i2 int) insn
}

// Name of exchange instruction.
func (_ insnEXchange) name() string {
	return "x"
}

// Generate an exchange instruction.
func (_ insnEXchange) gen(i1, i2 int) insn {
	ix := insnEXchange{i1, i2}
	return &ix
}

// Name of partner instruction.
func (_ insnPartner) name() string {
	return "p"
}

// Generate a partner instruction.
func (_ insnPartner) gen(i1, i2 int) insn {
	ix := insnPartner{prog(i1), prog(i2)}
	return &ix
}

// Given a Cauchy 1-row permutation table, generate a list
// of exchange (transposition) instructions that will
// produce this permutation. Works by tracing non-trivial
// cycles and decomposing them into transpositions.
func swapPermute(perm []int, xg xgen) *list.List {
	if tracePermute {
		// See the table coming in.
		fmt.Println(xg.name(), perm)
	}
	// Start a new instruction list.
	insns := list.New()
	// Mark table elements already processed.
	marks := make([]bool, len(perm))
	// Cover the entire table.
	for i := range perm {
		// Copy the index to avoid messing with loop
		// variable.
		j := i
		// We have now processed this index
		// (regardless of whether it had already
		// been previously processed).
		marks[j] = true
		// The next index to process.
		k := perm[j]
		// Note that if this cycle has been marked,
		// index k will have been marked also. Note
		// also that trivial cycles will not be
		// processed, since index k will have been
		// marked just above.
		for !marks[k] {
			if tracePermute {
				// Show generated exchange.
				fmt.Println(xg.name(), j, k)
			}
			// Generate and save the exchange
			// instruction.
			swap_jk := xg.gen(j, k)
			insns.PushBack(&swap_jk)
			// The table element has been
			// processed.
			marks[k] = true
			// Advance in table.
			j = k
			k = perm[j]
		}
	}
	return insns
}

// Assemble and optimize a list of instructions given as
// line strings.
func compileInsns(order int, insns []string) []insn {
	// Assemble the instructions.
	minsns := list.New()
	for _, v := range insns {
		switch v[0] {
		case 's':
			// Spin instruction.
			count := aoc.Atoi(v[1:])
			inx := insnSpin{count}
			minsns.PushBack(&inx)
		case 'x':
			// EXchange instruction.
			parse := strings.Split(v[1:], "/")
			i := aoc.Atoi(parse[0])
			j := aoc.Atoi(parse[1])
			inx := insnEXchange{i, j}
			minsns.PushBack(&inx)
		case 'p':
			// Partner instruction.
			a := prog(int(v[1]) - int('a'))
			b := prog(int(v[3]) - int('a'))
			inx := insnPartner{a, b}
			minsns.PushBack(&inx)
		}
	}

	// Optimization pass:
	// * Merge all the spins.
	// * Compile the exchanges into a single permutation.
	// * Generate instructions for the exchange permutation.
	// 
	// Global spin.
	spin := 0
	// Global exchange permutation.
	exchange := make([]int, order)
	for i := range exchange {
		exchange[i] = i
	}
	// Global partner permutation.
	partner := make([]int, order)
	for i := range partner {
		partner[i] = i
	}
	// Do the optimizations.
	for i := minsns.Front(); i != nil; i = i.Next() {
		target := i.Value.(insn)
		switch tinsn := target.(type) {
		case *insnSpin:
			// Update the current spin.
			spin = (spin + tinsn.count) % order
		case *insnEXchange:
			// Update the exchange table, taking
			// the current spin into account.
			i := (tinsn.i + order - spin) % order
			j := (tinsn.j + order - spin) % order
			exchange[i], exchange[j] =
				exchange[j], exchange[i]
		case *insnPartner:
			// Update the partner table, which
			// is independent of the current
			// spin.
			a := int(tinsn.a)
			b := int(tinsn.b)
			partner[a], partner[b] =
				partner[b], partner[a]
		}
	}
	// Generate code for exchanges.
	ix := insnEXchange{-1, -1}
	xinsns := swapPermute(exchange, ix)
	// Generate code for partners.
	ip := insnPartner{prog(-1), prog(-1)}
	pinsns := swapPermute(partner, ip)
	// Build the instruction slice.
	var final_insns []insn
	for i := xinsns.Front(); i != nil; i = i.Next() {
		final_insns = append(final_insns, *i.Value.(*insn))
	}
	for i := pinsns.Front(); i != nil; i = i.Next() {
		final_insns = append(final_insns, *i.Value.(*insn))
	}
	// Tack a final spin on the end if needed.
	if spin != 0 {
		i := insnSpin{spin}
		final_insns = append(final_insns, &i)
	}

	// Return the slice.
	return final_insns
}

// Run the insns on the starting posn.
func runInsns(posns []prog, insns []insn) {
	for _, insn := range insns {
		insn.exec(posns)
	}
}

// Solve the problems.
func main() {
	part1 := aoc.Setup()
	// Get the instructions.
	progStr := aoc.ReadLine()
	insnStrs := strings.Split(progStr, ",")
	// Allow for short order for the examples.
	var order int
	if len(os.Args) <= 2 {
		order = 16
	} else {
		order = aoc.Atoi(os.Args[2])
	}
	// Compile the instructions.
	insns := compileInsns(order, insnStrs)
	// Solve the appropriate part.
	if part1 {
		// Run the insns and print the answer.
		posns := permId(order)
		runInsns(posns, insns)
		fmt.Println(permString(posns))
	} else {
		// What we'll be looking for to start the
		// next cycle.
		posns0 := permId(order)
		// Start the first cycle.
		posns := permId(order)
		// Keep cycling until back to the start
		// state. Keep the count: it's important.
		count := 0
		var record [][]prog
		for {
			// Remember the permutation.
			// XXX This is so needlessly gross.
			// Thanks, Go.
			posns_copy := make([]prog, order)
			copy(posns_copy, posns)
			record = append(record, posns_copy)
			// Permute again and stop on cycle.
			count++
			runInsns(posns, insns)
			if reflect.DeepEqual(posns, posns0) {
				break
			}
		}
		// Now just return the recorded permutation
		// at the partial cycle position.
		iters := 1000000000 % count
		// Show the result.
		fmt.Println(permString(record[iters]))
	}
}
