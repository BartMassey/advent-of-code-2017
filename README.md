# Advent Of Code 2017: Tutorial Solutions in Go
Copyright (c) 2017 Bart Massey

Herein lie solutions to the problems of the 2017
[Advent of Code](http://adventofcode.com). Advent of Code is
a fantastic exercise, and I thank the author and others
involved profusely for their excellent work. Thanks also to
`relsqui` for pointing me at this back in 2015.

The solutions are in directories named `day01` through
`day25`. For each solution, I have included commented and
cleaned-up Go code. There is a `README.md` in every problem
directory containing descriptions, comments and usage
instructions. I have also included the problem descriptions
(`part1.md` and `part2.md`) and my specific `input.txt` for
posterity.

Highlights of this repo:

* Day 3: Nice closed-form solution for Part 1, with use of a
  map for easy Part 2 implementation.

* Day 5: Rust and Haskell implementations for performance
  comparison.

* Day 12: Nice implementation of fast union-find for
  an efficient solution.

* Day 16: Compiled out the permutations to get a solution
  that requires about 1500 machine instructions total.

* Day 17: Extra optimization step to get fast Part 2.

* Day 20: Gave a closed-form integer physics model for the
  system as part of the Part 2 solution. Includes a 4-page
  whitepaper describing the model.

* Day 23: Suggested a sequence of plausible compiler
  optimzations which could get Part 2 answered by
  a compiler rather than a human.

There is also a `src/aoc` directory containing a few
libraries used by solutions. `aoc.go` contains input
processing code. `math.go` contains some basic integer math
functions. The `knothash` subdirectory includes hashing code
used by Days 10 and 14.

I assume you have Go running on a fast-ish UNIX box with a
bunch of memory (although most everything should also work
on other operating systems). Before trying to run anything,
you'll need to set the GOPATH environment variable to the
root of this repo, because Go.

There are no special tests written for this code. I regard
passing both parts of a day's problem as strong validation.
Tests should get written.

The goals of these solutions are to:

* Provide canonical correct solutions with reasonable
  runtimes.

* Illustrate reasonable solution strategies.

* Illustrate the use of Go in problem-solving.

I expect to learn a ton of Go and a little bit of software
engineering I should already have known writing these.

There's also some engineering infrastructure in the form of
the `template` directory, the `mkday.sh` and
`process-aoc.sh` shell scripts and the `aoc` module.  This
sped up each day's setup considerably. At the beginning of
day 1 I would "`sh mkday.sh 1`". (On subsequent days, the
day number will be tracked automatically and can be
omitted.) At the end of the day I would select and copy the
page source of the day 1 AoC page and then

    xclip -selection CLIPBOARD -out | sh ../process-aoc.sh

to get markdown into the problem files for posterity.

You can get times for all parts of all days with "sh
times.sh". This also verifies that everything builds.

These solutions deserve a much more thorough top-level
description than I have the energy to write at this point.
I will revise this file in the indefinite future.

---

This work is licensed under the "MIT License".  Please see
the file `COPYING` in the source distribution of this software
for license terms.
