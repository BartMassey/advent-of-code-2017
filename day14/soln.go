// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 14.

package main

import (
	"aoc"
	"aoc/knothash"
	"fmt"
)

// Turn the hashes into a "bitmap": an array of booleans.
func explode(hashes [][16]uint8) [][]bool {
	var bitmap [][]bool
	// Build the bitmap a row at a time.
	for _, hash := range hashes {
		var rowmap []bool
		// Build the row a byte at a time.
		for _, byte := range hash {
			// Append each bit of the byte to the row.
			for k := 7; k >= 0; k-- {
				// Careful! Work high-to-low-bit.
				bit := (byte >> uint(k)) & 1
				rowmap = append(rowmap, bit == 1)
			}
		}
		bitmap = append(bitmap, rowmap)

	}
	return bitmap
}

// Return the number of set bits in the bitmap.
func popCount(bitmap [][]bool) int {
	pop_total := 0
	for _, row := range bitmap {
		for _, bit := range row {
			if bit {
				pop_total++
			}
		}
	}
	return pop_total
}

// Return the number of contiguous set-bit regions that were
// in the bitmap. Clears the bitmap as a side-effect.
func regionCount(bitmap [][]bool) int {
	// Dimensions of bitmap.
	height := len(bitmap)
	width := len(bitmap[0])

	// Turn off a set bit and all of its neighbors
	// recursively.  This will flood-clear a contiguous
	// region. XXX Because flood is local and recursive
	// it needs to be declared before initialization to
	// be visible during initialization.
	var flood func(i, j int)
	flood = func(i, j int) {
		// Base case: nothing here.
		if !bitmap[i][j] {
			return
		}
		// Recursive case: clear this bit, then
		// flood-clear its neighbors.
		bitmap[i][j] = false
		// List of neighbor offsets.
		deltas := [] struct { i, j int } {
			{-1, 0},
			{1, 0},
			{0, -1},
			{0, 1},
		}
		// Try to flood each neighbor.
		for _, delta := range deltas {
			di := delta.i + i
			dj := delta.j + j
			// Clip on all sides because it's
			// easier.
			if di < 0 || di >= height {
				continue
			}
			if dj < 0 || dj >= width {
				continue
			}
			// Not clipped, so flood.
			flood(di, dj)
		}
	}

	// Count of cleared regions.
	regions := 0
	// Look in each successive position for an uncleared
	// bit. If found, flood-clear the region containing
	// that bit and increment the count.
	for i := range bitmap {
		for j := range bitmap[i] {
			if bitmap[i][j] {
				flood(i, j)
				regions++
			}
		}
	}
	return regions
}

// Solve the problems.
func main() {
	part1 := aoc.Setup()
	key_prefix := aoc.ReadLine()
	// Build list of hashes to process.
	var hashes [][16]uint8
	for i := 0; i < 128; i++ {
		key := key_prefix + "-" + fmt.Sprint(i)
		hashes = append(hashes, knothash.KnotHash(key))
	}
	// Convert the hashes to a bitmap.
	bitmap := explode(hashes)
	if part1 {
		// Print the count of set bits.
		pop_total := popCount(bitmap)
		fmt.Println(pop_total)
	} else {
		// Print the count of cleared regions.
		region_count := regionCount(bitmap)
		fmt.Println(region_count)
	}
}
