# Advent of Code 2017: Day 14
Copyright (c) 2017 Bart Massey

OK, popcount. That's straightforward. I'll just copy and
paste stuff from Day 10 into my program (for now) and write
some glue. Finished Part 1 at position 400-something. Yay
me.

Part 2 is flood-fill. OK, I wrote that thirty years ago in
Pascal without any references. (Wrote a graphics editor for
my 8-bit CP/M S-100 box.) I can do it again. Type type type
and…

So many bugs. Part 2 took almost two hours because my
debugging went badly.

The underlying problem was that I didn't read the spec
carefully enough, and took the bits of each byte of the hash
in little-endian order. Which is dumb of me anyway, but an
easy mistake to make. Of course this does not affect the
Part 1 answer, so I thought I was fine.

When Part 2 gave a wrong answer on the test case, I figured
I had a minor bug in my flood fill algorithm (which indeed I
might have — who knows). So I spent a lot of time trying to
"debug" working code (always a terrible plan). I eventually
concluded that my debugged fill code had to be right. I read
the spec carefully and found and fixed the bit-endianness
bug. However, I still wasn't getting the right answer for
Part 2 on the test case.

Eventually (after way too long) I noticed that I should be
printing and checking the graphics corresponding to the test
case. When I did, I got completely wrong output. Nothing
made sense. I fiddled around with it for a half-hour or so,
working on the instrumentation and adding checks.

I finally noticed that I was repeatedly running on the
problem input and checking the answer against the test
output. Thanks shell history. Running on the test output
gave me the right answer for the test, and the answer for
Part 2 turned out to be the "wrong" number I'd been staring
at for an hour. 

Probably time to automate the testing uniformly. I'll have
to look into it.

Still finished at position 800-something, so
apparently this one is more challenging that previous days
for people.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
