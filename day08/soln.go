// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 8.

package main

import (
	"aoc"
	"fmt"
	"regexp"
)

// Format of an instruction line.
var insnRE *regexp.Regexp =
	regexp.MustCompile(`^([a-z]+) (inc|dec) (-?[0-9]*) if ([a-z]+) (<|>|==|!=|>=|<=) (-?[0-9]*)$`)

// Evaluate a numeric test.
func test_cond(l int, op string, r int) bool {
	switch op {
	case "==":
		return l == r
	case "!=":
		return l != r
	case "<=":
		return l <= r
	case ">=":
		return l >= r
	case "<":
		return l < r
	case ">":
		return l > r
	}
	panic("bad op")
}

// Solve the problem and show the answer.
func main() {
	// Get set up.
	part1 := aoc.Setup()
	// State of the registers.
	registers := map[string]int {}
	// Largest register value seen so far.
	adj_max := 0
	// Read and process instructions.
	for aoc.ScanLine() {
		line := aoc.NextLine()
		// Get instruction parse.
		pats := insnRE.FindStringSubmatch(line)
		if pats == nil {
			panic("syntax error")
		}
		target := pats[1]
		dirn := pats[2]
		// These conversions are checked by the regex.
		amt := aoc.Atoi(pats[3])
		test := pats[4]
		cond := pats[5]
		val := aoc.Atoi(pats[6])
		// Only run the instruction if needed.
		if test_cond(registers[test], cond, val) {
			// Positive dec is negative inc.
			if dirn == "dec" {
				amt = -amt
			}
			// Adjust the register.
			registers[target] += amt
			// Update the max register value.
			if registers[target] > adj_max {
				adj_max = registers[target]
			}
		}
	}
	aoc.LineErr()
	// Find the largest current register value. Must
	// be careful not to introduce new zero registers.
	inited := false
	var max int
	for _, v := range registers {
		// On first iteration, unconditionally set value.
		if !inited {
			inited = true
			max = v
			continue
		}
		// On other iterations, update value.
		if v > max {
			max = v
		}
	}
	// XXX We compute both part's answer on every run.
	if part1 {
		fmt.Println(max)
	} else {
		fmt.Println(adj_max)
	}
}
