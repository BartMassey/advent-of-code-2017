# Advent of Code 2017: Day 8
Copyright (c) 2017 Bart Massey

Mostly an exercise in getting the parsing regexp in quickly,
for which I got to practice yesterday. Mostly
straightforward, with only one or two gross bits. Best daily
ranking so far.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
