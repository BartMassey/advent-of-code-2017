\documentclass[12pt]{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{blockstyle}

\title{Advent of Code 2017 Day 20:\\
  Integer Particle Motion}
\author{Bart Massey}
\date{2017-12-20}

\begin{document}
\maketitle

Day 20 of Advent of Code 2017 treats motion in a
pseudo-Newtonian particle system. Particle motion
is modeled on Newtonian mechanics, but limited to
integral positions, velocities and accelerations.

The model treats three-dimensional particles, and gives the
equation of motion for a particle with initial vector coordinates
$(x_0, v_0, a_0)$ as \begin{eqnarray}
  v_{t+1}&~\leftarrow~&v_t + a_0\\
  x_{t+1}&~\leftarrow~&x_t + v_{t+1}
\end{eqnarray}

\section*{Part 1}

Part 1 asks which particle in an ensemble stays the nearest
to the origin in the large-time limit. The distance model is
the $L_1$ norm (sum of absolute value of coordinates),
presumably chosen because it has an integer result.

A bit of reflection reveals that in the limit the
acceleration must dominate, so the particle with smallest
$L_1$ acceleration should be selected. If there are ties,
they should be broken in turn by $L_1$ velocity and $L_1$
position.

\section*{Part 2}

Part 2 asks a more difficult question. Assume that if two or
more particles are at the same integer coordinate at the
same time, they annihilate each other. In the long-time
limit, how many particles will remain?

An na\"ive approximation is to use a computer to time
step the system forward using the particle motion
equations. At each step, colliding particles can be
annihilated. When the system seems to have stabilized in the
sense that no more particles are colliding, stop it and
report the number of remaining particles.

For the given instance, this approximation returns the right
answer. But it calls for the question: when may one stop the
simulation? Too soon, and one risks giving a too-high
answer.

I couldn't spot an easy generalization of Part 1 that got me
there. One can consider pairs of particles from the
ensemble, and note when they diverge by considering relative
acceleration, velocity and position in a manner similar to
Part 1. However, one has to be careful to not collide
collections of particles one of which had already been
annihilated, so there's the potential for an $O(n^3)$
algorithm where $n$ is the number of particles: that's no
good for the 1000 particles in the given instance.

Instead, let's start by deriving the equations of particle motion in
closed form.

\section*{Equations of Motion}

Here are the equations of motion for one coordinate of a particle.

\begin{eqnarray}
  v_{t+1} &=& v_t + a_0 \\
      v_t &=& v_0 + \sum_{i=1}^t{a_0} = v_0 + a_{0}t \\*[1.5\baselineskip]
  x_{t+1} &=& x_t + v_{t+1} \\
          &=& x_0 + \sum_{i=1}^{t}\left(v_0 + a_{0}i\right) \\
          &=& x_0 + v_0t + a_0\sum_{i=1}^{t}{i} \\
          &=& x_0 + v_0t + \frac{a_{0}t(t+1)}{2} \label{eqn-sim}\\
          &=& x_0 + \left(v_0 + \frac{a_0}{2}\right)t + \frac{a_0}{2}t^2
\end{eqnarray}

To check this work, Equation~\ref{eqn-sim} was successfully
tested against simulation via a Go prototype.

\section*{Collisions}

Having seen how a particle moves, the next relevant
observation is that two particles will collide when their
$x$, $y$ and $z$ position coordinates all simultaneously
agree: that is, when the difference in position coordinates
is zero. Because the position coordinates are just
polynomials, we can construct a new virtual particle whose
position polynomial coefficients are the difference of the
target particle's cooefficients. If this virtual particle
ever hits the origin, the two particles have collided.

Working again without loss of generality on just the $x$
coordinate, we need \begin{eqnarray}
   x_t &=& 0 \\
   \frac{a_0}{2}t^2 + \left(v_0 + \frac{a_0}{2}\right)t + x_0 &=& 0 \\
   a_{0}t^2 + \left(2v_0 + a_0\right)t + 2x_0 &=& 0 \label{eqn-norm}
\end{eqnarray}
where Equation~\ref{eqn-norm} has been multiplied by two
to get rid of the fractions that would make things confusing
in an environment where all solutions must be integers.

The zeros of Equation~\ref{eqn-norm} can be found in the
standard way using the quadratic formula. Let $$
A=a_0~~~B=2v_0+a_0~~~C=2x_0
$$
and we find that the particle hits the origin at times \begin{eqnarray}
  t &=& \frac{-B \pm \sqrt{B^2 -4AC}}{2A} \\
  t &=& \frac{-2v_0-a_0 \pm \sqrt{\left(2v_0+a_0\right)^2-8a_0x_0}}{2a_0}
  \label{eqn-quad}
\end{eqnarray}

Equation~\ref{eqn-quad} imposes a number of constraints on
the zero.
\begin{enumerate}
\item The discriminant (inside the square root) must be
  a perfect square.
\item The numerator must be positive (since we
  don't care about negative times).
\item The numerator must be evenly divisible by the
  denominator. \label{enum-nconditions}
\item The denominator must not be zero. If it is,
  we find the zero using just position and velocity
  using a linear equation. If the velocity is also zero, the
  there is either no zero (if the position is not zero) or
  there is a zero for every time (the position is zero).
\end{enumerate}

All of these conditions can be checked programatically as
shown in Figure~\ref{fig-collisiont}. If
all of a virtual particle's $x$, $y$ and $z$ position
coordinates are zero at the same time $t$, the particles
collide at that time.

\begin{figure}[ht]
  \begin{center}\parbox{0pt}{
      \input{collidet.tex}
  }\end{center}
  \caption{Set of collision times for a virtual particle.}
  \label{fig-collisiont}
\end{figure}

A further complication in computing all this is that once a
particle has been annihilated, it cannot take part in any
future collisions. One idea, then, is to compute all the
pairwise annihilations and put the pairs in a min priority queue
orderd by annihilation time. Extracted pairs are marked as
annihilated: if at least one in an extracted pair is already
annihilated, the collision is ignored.

Sadly, the approach given above does not quite work, because
three or more particles may collide at the same time. We
will need to build a set of collision descriptions, where
each collision description contains a time and the set of
particles that will collide at that time. We can then modify
our queueing approach to mark the whole collision set in a
description as annihilated iff at least two of the particles
in the set are not already annihilated.

\section*{Bounding A Simulation}

In my case, a system simulation has already been written and
performs correctly. Really, all that is needed is a bound on
the running time of that simulation. Thus, one can simply
compute the collision times for all pairs of particles,
ignoring annihilation, and take the maximum time thus
computed as the simulation runtime. This is guaranteed to be
a safe bound.

\end{document}
