// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Prototype for motion model.

package main

import "fmt"

// Step the position and velocity forward one timestep.
func step(x, v, a int) (int, int) {
	v += a
	x += v
	return x, v
}

// Obtain the position at time t directly from the initial
// parameters.
func direct_x(x0, v0, a, t int) int {
	return x0 + v0*t + a*t*(t+1) / 2
}

// Compare the incremental and direct models at some
// timesteps.
func main() {
	// Initial position and velocity.
	x0 := 3
	v0 := 5
	// Acceleration is a constant.
	const a = 17
	// Position and velocity at timestep.
	x := x0
	v := v0
	// Try 100 steps.
	for t := 1; t <= 100; t++ {
		// Compute using step model.
		x, v = step(x, v, a)
		// Compute using direct model.
		xd := direct_x(x0, v0, a, t)
		// Check for difference.
		if x != xd {
			fmt.Println("test failed", t, x, xd)
			return
		}
	}
	fmt.Println("test passed")
}
