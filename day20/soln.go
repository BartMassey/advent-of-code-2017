// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 20.

package main

import (
	"aoc"
	"fmt"
	"regexp"
	"strings"
)

// An x/y/z coordinate.
type coord [3]int

// A particle.
type particle struct {
	// Identifier.
	id int
	// Status (live or annihilated).
	dead bool
	// 3D position, velocity and acceleration coordinates.
	c [3]coord
}

// L1 (absolute/taxicab) distance of c from origin.
func coordDist(c *coord) int {
	s := 0
	for _, v := range c {
		s += aoc.Abs(v)
	}
	return s
}

// Return true iff particle p1 will be nearer the origin
// than particle p2 in the long-time limit.
func nearerInLimit(p1, p2 *particle) bool {
	// Try acceleration, then velocity, then position.
	for k := 2; k >= 0; k-- {
		v1 := coordDist(&p1.c[k])
		v2 := coordDist(&p2.c[k])
		if v1 < v2 {
			// Found an ordering.
			return true
		} else if v1 > v2 {
			// Found an ordering.
			return false
		}
	}
	// Particles are equal.
	return false
}

// Return the id of the input particle that will stay
// closest to the origin.
func soln1(inputs []*particle) int {
	i_min := 0
	for i := 1; i < len(inputs); i++ {
		if nearerInLimit(inputs[i], inputs[i_min]) {
			i_min = i
		}
	}
	return inputs[i_min].id
}


// Wrapper for map[].
type pos struct {
	c coord
}

// Return a list of times which correspond to zeros in the
// motion equation for the 1D-particle (coordinate) with the
// given position, velocity and acceleration. Returns nil if
// all times are valid (stuck at 0), and an empty list if no
// times are valid.
func coordCollideTimes(x, v, a int) []int {
	// List of results.
	r := make([]int, 0)
	// Special case: not accelerating.
	if a == 0 {
		// Special case: not moving.
		if v == 0 {
			// If we are at 0 we always will be.
			if x == 0 {
				return nil
			}
			// We will never meet.
			return r
		}
		// Compute potential collision time.
		t := x / v
		// If t is negative (collision in past) or
		// non-integer (collide between ticks)
		// then no collision to report.
		if t < 0 || aoc.Abs(x) % aoc.Abs(v) != 0 {
			return r
		}
		// Record and report collision.
		r = append(r, t)
		return r
	}
	// B term in quadratic formula.
	b := 2 * v + a
	// Discriminant in quadratic formula.
	d := b * b - 8 * a * x
	// Negative discriminant means imaginary solution.
	if d < 0 {
		return r
	}
	// Not perfect square discriminant means irrational
	// solution.
	ds := aoc.ISqrt(d)
	if ds * ds != d {
		return r
	}
	// Iterate over numerators of quadratic formula.
	// Will append a double root twice.
	ns := []int{-b - ds, -b + ds}
	for _, n := range ns {
		// Quadratic formula root.
		t := n / (2 * a)
		// No negative or non-integer roots allowed.
		if t < 0 || aoc.Abs(n) % aoc.Abs(2 * a) != 0 {
			continue
		}
		// Remember the root.
		r = append(r, t)
	}
	// Return found roots.
	return r
}

// Intersect two lists of times. Handle the special case of
// nil == anytime.
func intersectTimes(ts1, ts2 []int) []int {
	// If ts1 is anytime, ts2 is it.
	if ts1 == nil {
		return ts2
	}
	// If ts2 is anytime, ts1 is it.
	if ts2 == nil {
		return ts1
	}
	// Build up result list. Important not to return
	// nil.
	r := make([]int, 0)
	for i := range ts1 {
		for j := range ts2 {
			if ts1[i] == ts2[j] {
				r = append(r, ts1[i])
				break
			}
		}
	}
	return r
}

// Find the possible collision times (if any) of p1 and p2.
func particleCollideTimes(p1, p2 *particle) []int {
	// List of currently-valid times. Starts with
	// all times valid.
	var r []int
	// Need to check x, y and z.
	for i := range p1.c[0] {
		// Position, velocity and acceleration.
		var vc [3]int
		for j := range p1.c {
			// Virtual coordinate for difference
			// particle.
			vc[j] = p1.c[j][i] - p2.c[j][i]
		}
		// Find the collide times for this virtual
		// coordinate.
		ts := coordCollideTimes(vc[0], vc[1], vc[2])
		// Intersect with current satus.
		r = intersectTimes(r, ts)
	}
	if r == nil {
		// Pauli Exclusion Principle.
		panic("PEP violation")
	}
	// Return the answer.
	return r
}

// Find the maximum possible collision time over all pairs
// of particles.
func maxCollideTime(inputs []*particle) int {
	t_max := 0
	for i := range inputs {
		for j := i + 1; j < len(inputs); j++ {
			ts := particleCollideTimes(inputs[i], inputs[j])
			for _, t := range ts {
				t_max = aoc.Max(t, t_max)
			}
		}
	}
	return t_max
}

// Return the number of particles left after the last
// collision.
func soln2(inputs []*particle) int {
	// How many particles are left.
	count := len(inputs)
	// How long to run the simulation.
	tsim := maxCollideTime(inputs)
	// Run the simulation.
	for i := 0; i <= tsim; i++ {
		// Create a new map from position to
		// particle count.
		positions := make(map[pos]int)
		// Update each particle.
		for _, v := range inputs {
			// Skip dead particles.
			if v.dead {
				continue
			}
			// Update velocity, then position.
			for i := 2; i >= 1; i-- {
				// Update x, y and z.
				for j := range v.c[i] {
					v.c[i-1][j] += v.c[i][j]
				}
			}
			// Wrap the position to use as an
			// index.
			x := pos{v.c[0]}
			// Bump the particle count for the
			// position.
			positions[x]++
		}
		// Mark all particles involved in any
		// collision as dead.
		for _, v := range inputs {
			// Can skip already-dead particles.
			if v.dead {
				continue
			}
			// Wrap the position to use as an
			// index.
			x := pos{v.c[0]}
			// If two or more live particles are
			// at this position, this particle
			// is dead.
			if positions[x] >= 2 {
				v.dead = true
				count--
			}
		}
	}
	return count
}


// Solve the problems.
func main() {
	part1 := aoc.Setup()
	// Format of an input line, with the @ representing
	// an integer.
	pat := "^p=<@,@,@>, v=<@,@,@>, a=<@,@,@>$"
	// Expand @ to integer pattern and compile.
	re := strings.Replace(pat, "@", " ?(-?[0-9]+)", -1)
	pRE := regexp.MustCompile(re)
	// Read the particles.
	var inputs []*particle
	for aoc.ScanLine() {
		desc := aoc.NextLine()
		// Break the description into fields.
		matches := pRE.FindStringSubmatch(desc)
		// Build up a new particle.
		var p particle
		p.id = len(inputs)
		// Set up the components, using ugly
		// hand-indexing.
		for i := 0; i < 3; i++ {
			for j := 0; j < 3; j++ {
				p.c[i][j] = aoc.Atoi(matches[1+3*i+j])
			}
		}
		// Save the particle.
		inputs = append(inputs, &p)
	}
	aoc.LineErr()
	if part1 {
		fmt.Println(soln1(inputs))
	} else {
		fmt.Println(soln2(inputs))
	}
}
