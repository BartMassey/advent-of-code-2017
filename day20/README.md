# Advent of Code 2017: Day 20
Copyright (c) 2017 Bart Massey

OK, this was just gross from start to finish.

It was obvious enough for Part 1 that we could just do a
big-Omega distance analysis to figure out which function
grew the slowest.

I chose to sort the particles by big-O distance and take the
smallest, to learn about Go's sorting interface. Turns out
it's terrible: you have to define a bunch of extra auxiliary
functions to do a sort, including the swap. Should have just
taken the minimum in the first place, so I simplified the
code to do that.

For Part 2 the "obvious" thing to do is to simulate the
particle motions, deleting particles as they collide, until
a stopping criterion is reached. Sadly, the obvious stopping
criterion is distance and that doesn't work in any obvious
way, since particles may be moving at the same rate along
the same line but along different paths. Also, detecting
collisions takes a bit of thought to get right: a map of
positions to counts is involved.

Originally I "solved" Part 2 the way most folks likely did:
print the particle count at each iteration of the simulation
and wait until it seems to be spitting out the same count
over and over. Not really a solution. 

I then spent about eight hours (maybe more) on doing Part 2
right. The paper with filename `motion.pdf` in the
motion-paper subdirectory gives the details. The basic
strategy was to first find the time of the last possible
collision using the equations of motion of the particles,
then run the simulation forward that many steps.

Go was not mostly my friend here. Native sets and a native
option type would have been really helpful. I also learned
that

    x = append(x,)

is syntactically valid (I guess `append` is varargs?) and
produces no runtime error. So there's that. Hard typo to
find.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
