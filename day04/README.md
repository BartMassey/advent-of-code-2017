# Advent of Code 2017: Day 4
Copyright (c) 2017 Bart Massey

This was scarily easy algorithmically. The only catch was,
as usual, Go, a language about which I am beginning to have
serious doubts. I can find no convenient way to sort the
characters of a string. That seems like a problem.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
