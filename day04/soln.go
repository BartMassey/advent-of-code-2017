// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 4.

package main

import (
	"aoc"
	"fmt"
	"sort"
	"strings"
	"unicode"
)

// Solve the problem by walking the lines looking for a valid one.
func main() {
	// Set up and initialize the accumulator.
	part1 := aoc.Setup()
	nValid := 0
	// Each line is treated independently.
	for aoc.ScanLine() {
		// Read and split an input line.
		line := aoc.NextLine()
		row := strings.FieldsFunc(line, unicode.IsSpace)
		// Check the passphrase currently in row.
		validate := func() bool {
			words := map[string] bool {}
			for _, w := range row {
				// For Part 2, canonicalize w.
				if !part1 {
					// Sort letters of w.
					// https://stackoverflow.com/a/22689818
					rs := strings.Split(w, "")
					sort.Strings(rs)
					w = strings.Join(rs, "")
				}
				// If we've already seen it, we're done.
				if words[w] {
					return false
				}
				// Now we've seen it.
				words[w] = true
			}
			// Never saw the same word twice.
			return true
		}
		// If it's good, count it.
		if validate() {
			nValid++
		}
	}
	// Check for read errors.
	aoc.LineErr()
	// Show the final count.
	fmt.Println(nValid)
}
