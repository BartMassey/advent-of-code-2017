# Advent of Code 2017: Day 23
Copyright (c) 2017 Bart Massey

Part 1 was a pretty straightforward copy-paste from
Day 18. Ugly, but there it is.

I almost gave up on Part 2. *Almost.*

It wasn't like I didn't see what is wanted. It just isn't a
programming problem. It's a reverse-engineering or
security-analysis problem. Besides, I had a rule that my
program would always work by reading the raw input provided,
with no transformation.  Here I'd have to win some kind of
Turing award for clever compilation to do that, so I ended
up having to break my rule and translate the input by
hand. Uggh.

Because I'm compulsive I wrote an annotated version of the
assembly code (see `input-annote.txt`), untangled the
inputs, and then started hand-translating into C. (Why C
instead of Go? I don't know. I went back and fixed it for
the final release: it's now Go buried in the `soln.go`
program.) In the process, I found the C compiler doing some
*amazing* things: warning me about undefined behaviors that
would happen after huge numbers of iterations of loops.

I finally untangled enough stuff that it was pretty obvious
that we were looking for composite numbers of the form
*b+17n* in the range *b..c*. The primality test was
super-pessimized, so replacing it with dumb trial division
in full range was enough to speed it up to run quickly.  I
then used the unoptimized Go version for Part 1 and the
optimized Go version for Part 2, disregarding the original
input entirely.

Note that automation of Part 2 optimization is in principle
possible. See `optimization.md` and `soln-opt.go`.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1
    go run soln.go 2

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
