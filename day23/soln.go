// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 23.

package main

import (
	"aoc"
	"fmt"
)

// Transliteration of input assembly code to Go.
// Unoptimized, and counts and returns number of multiplies.
func soln1() int {
	mul_count := 0
	b := 65
	c := 65
	h := 0
	for {
		f := true
		// Very inefficient primality test for b.
		// The f flag is set according to whether b
		// is prime.
		d := 2
		for {
			e := 2
			for {
				mul_count++
				if d * e == b {
					f = false
				}
				e++
				if e == b {
					break
				}
			}
			d++
			if d == b {
				break
			}
		}
		// Bump the count of composites if b is
		// composite.
		if (!f) {
			h++
		}
		if b == c {
			break
		}
		b += 17
	}
	return mul_count
}

// Optimization of soln1().  Returns number of composites of
// the form b + 17n in the range b..c.
func soln2() int {
	b := 106500
	c := 123500
	h := 0
	for {
		f := true
		// Replacement composite test using trial
		// division.
		for q := 2; q < b; q++ {
			if b % q == 0 {
				f = false
				break
			}
		}
		if (!f) {
			h++
		}
		if b == c {
			break
		}
		b += 17
	}
	return h
}

// Solve the problems.
func main() {
	part1 := aoc.Setup()
	if part1 {
		mul_count := soln1()
		fmt.Println(mul_count)
	} else {
		h_val := soln2()
		fmt.Println(h_val)
	}
}
