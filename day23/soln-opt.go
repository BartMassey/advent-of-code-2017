// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 23 Part 2, optimized.

package main

import (
	"fmt"
)

// Optimization of naive Go code.  Returns number of
// composites of the form b + 17n in the range b..c.
func soln2() int {
	b := 106500
	c := 123500
	h := 0
	for {
		f := true
		d := 2
		for {
			e := b / d
			if e < d {
				break
			}
			// for {
			// 	t := d * e
			// 	if t >= b {
			// 		if t == b {
			// 			f = false
			// 		}
			// 		break
			// 	}
			// 	e++
			// }
			if b % d == 0 {
				f = false
				break
			}
			d++
			if d == b {
				break
			}
		}
		if (!f) {
			h++
		}
		if b == c {
			break
		}
		b += 17
	}
	return h
}

// Solve the problems.
func main() {
	h_val := soln2()
	fmt.Println(h_val)
}
