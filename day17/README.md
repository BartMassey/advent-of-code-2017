# Advent of Code 2017: Day 17
Copyright (c) 2017 Bart Massey

Interesting in that Part 1 and Part 2 require relatively
different solutions, even though they are similar problems.

Part 1 is a fairly straightforward exercise in insertion
into a circularly-linked list. Took some debugging and
care to get the details right, but nothing epic. Go's pointer
types worked with a minimum of grief.

Part 2 requires the insight that the only buffer insertions
that needs to be tracked are those at position 0: the rest
of the buffer doesn't matter. So there's some simple modulo
arithmetic to be run 50 million times.

My initial Go implementation of Part 2 ran in a half-second,
but I couldn't help but optimize it further. Since
insertions that don't "wrap around" in the buffer can't
possibly do anything, we can do a division and skip forward
a whole bunch of rounds at once. Went from 50 million
iterations to about 10 thousand: that should be fast enough
even for Python, if one had the hankering.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
