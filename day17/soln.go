// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 17.

package main

import (
	"aoc"
	"fmt"
)

// Element of a circularly-linked list of ints.
type elem struct {
	val int
	next *elem
}

// Solve the problems.
func main() {
	part1 := aoc.Setup()
	nstep := aoc.Atoi(aoc.ReadLine())
	if part1 {
		// Set up the initial buffer.
		buffer := elem{0, nil}
		buffer.next = &buffer
		cur := &buffer
		// Run the remaining rounds.
		for round := 1; round <= 2017; round++ {
			// Step to the correct position.
			for step := 0; step < nstep; step++ {
				cur = cur.next
			}
			// Make and insert the new element.
			new := elem{round, cur.next}
			cur.next = &new
			// Advance to the new element.
			cur = cur.next
		}
		// Show the next value.
		fmt.Println(cur.next.val)
	} else {
		// Position in the buffer.
		cur := 0
		// Last value inserted after position 0.
		insval := -1
		// Run the remaining rounds. Note that the
		// length of the buffer will be equal to the
		// number of rounds run.
		nrounds := 50000000
		for round := 1; round <= nrounds; round++ {
			// Try to skip past a bunch of
			// irrelevant steps.
			//
			// There's still some room for
			// optimization here using a fancy
			// Gauss's Sum calculation to take
			// into account the extra
			// insertions, but yuck.
			safe_steps := (round - cur) / (nstep + 1)
			if safe_steps > 1 {
				// Skip ahead a bunch of steps.
				cur += safe_steps * (nstep + 1)
				round += safe_steps - 1
				continue
			}
			// Advance cur to the next insertion
			// point.
			cur = (cur + nstep + 1) % round
			// If inserting after position 0,
			// remember inserted value.
			if cur == 0 {
				insval = round
			}
		}
		// Show the last value inserted after position 0.
		fmt.Println(insval)
	}
}
