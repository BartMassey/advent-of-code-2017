# Advent of Code 2017: Day 5
Copyright (c) 2017 Bart Massey

Wow that was easy. Too easy. Suspiciously easy. Less than 15
minutes start to finish, and I was in no real hurry. Didn't
even have any Go issues.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

In reaction to a Reddit thread talking about relative
performance of Rust and Haskell on this problem, I've also
included Rust and Haskell implementations of Part 2.

To compile and run the Rust implementation use

    rustc -O -o soln soln.rs
    ./soln <input.txt

To compile and run the Haskell implementation use

    ghc -O4 --make soln.hs
    ./soln <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
