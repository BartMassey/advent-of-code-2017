// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

//! Advent of Code 2017 Day 5 Part 2 in Rust.

use std::io::*;

fn main() {
    // Muh instruction table.
    let mut jumps: Vec<isize> = Vec::new();
    // Read lines of input into the sheet.
        let reader = BufReader::new(stdin());
    for line in reader.lines() {
        // Read and split an input line.
        let j: isize = line.expect("bad read").parse().expect("bad int");
        jumps.push(j)
    };
    let mut steps = 0;
    let mut pc = 0isize;
    // Run the program and display the result.
    while pc >= 0 && pc < jumps.len() as isize {
        // Get the jump insn.
        let offset = jumps[pc as usize];
        // Calculate the next PC.
        let next_pc = pc + offset;
        // Adjust the jump insn.
        if offset < 3 {
            jumps[pc as usize] += 1
        } else {
            jumps[pc as usize] -= 1
        };
        // Move the PC.
        pc = next_pc;
        // That's a step.
        steps += 1
    };
    println!("{}", steps)
}
