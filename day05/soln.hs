-- Copyright © 2017 Bart Massey
-- This program is licensed under the "MIT License".
-- Please see the file COPYING in this distribution
-- for license terms.

-- | Advent of Code 2017 Day 5 Part 2 in Haskell.

import System.IO
import qualified Control.Monad.ST as ST
import qualified Data.Vector.Unboxed as V
import qualified Data.Vector.Unboxed.Mutable as VM

-- | Actually run out the program.
doJumps :: V.MVector s Int -> Int -> Int -> ST.ST s Int
doJumps table loc count
    | loc >= 0 && loc < VM.length table = do
        -- Still in-bounds, so take the next step.
        offset <- VM.read table loc
        let adj = if offset < 3 then offset + 1 else offset - 1
        VM.write table loc adj
        doJumps table (loc + offset) (count + 1)
    | otherwise = return count

-- | Process input, solve, and show output.
solve :: String -> String
solve jumpStrings =
    let
        -- Jump list.
        jumps = map read $ lines jumpStrings
        -- Number of steps taken.
        count = ST.runST $ do
                  -- Jump table as mutable vector.
                  jumpTable <- V.thaw $ V.fromList jumps
                  -- Run it.
                  doJumps jumpTable 0 0
    in
    unlines $ [show count]

main :: IO ()
main = interact solve
