// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 5.

package main

import (
	"aoc"
	"fmt"
)

func main() {
	part1 := aoc.Setup()
	// Muh instruction table.
	var jumps []int
	// Read lines of input into the sheet.
	for aoc.ScanLine() {
		// Read and split an input line.
		line := aoc.NextLine()
		j := aoc.Atoi(line)
		jumps = append(jumps, j)
	}
	aoc.LineErr()
	// Run the program and display the result.
	pc := 0
	steps := 0
	for pc >= 0 && pc < len(jumps) {
		// Get the jump insn.
		offset := jumps[pc]
		// Calculate the next PC.
		next_pc := pc + offset
		// Adjust the jump insn.
		if part1 || offset < 3 {
			jumps[pc]++
		} else {
			jumps[pc]--
		}
		// Move the PC.
		pc = next_pc
		// That's a step.
		steps += 1
	}
	fmt.Println(steps)
}
