# Advent of Code 2017: Day 22
Copyright (c) 2017 Bart Massey

Pretty straightforward state machine exercise. Biggest trick
was understanding that a set or map of nodes by state would
work better than a 2D array here. The example/test was
really helpful, and Go did not get in the way.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
