// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 22.

package main

import (
	"aoc"
	"fmt"
)

// 2D coordinates in row-column order.
type coord [2]int

// The four cardinal directions.
type dirn int

// Node states.
type state int
const (
	S_CLEAN state = state(iota)
	S_WEAKENED
	S_INFECTED
	S_FLAGGED
)

// Coordinate delta for each cardinal direction.
var adjacent [4]coord = [4]coord{
	{-1, 0},   // North
	{0, -1},   // West
	{1, 0},    // South
	{0, 1},    // East
}

// The direction to the left of dir.
func leftTurn(dir dirn) dirn {
	return (dir + 1) % 4
}

// The direction to the right of dir.
func rightTurn(dir dirn) dirn {
	return (dir + 3) % 4
}

// The direction behind dir.
func reverseTurn(dir dirn) dirn {
	return (dir + 2) % 4
}

// Alter loc to move one step in direction dir.
func move(loc *coord, dir dirn) {
	delta := &adjacent[int(dir)]
	loc[0] += delta[0]
	loc[1] += delta[1]
}

// Solve the problems.
func main() {
	part1 := aoc.Setup()
	// Infection state of nodes.
	nodes := make(map[coord]state)
	// Largest row/column coordinates seen so far.
	r_max := 0
	c_max := 0
	// Read the map.
	for r := 0; aoc.ScanLine(); r++ {
		row := aoc.NextLine()
		for c, v := range row {
			switch v {
			case '#':
				nodes[coord{r, c}] = S_INFECTED
			case '.':
				// do nothing
			default:
				panic("bad map char")
			}
			c_max = aoc.Max(c_max, c)
		}
		r_max = r
	}
	aoc.LineErr()
	// Check to make sure that the map size is odd.
	// XXX Largest coordinate is at map size - 1.
	if r_max % 2 != 0 || c_max % 2 != 0 {
		panic("even map coordinate")
	}
	// Map center coordinates.
	loc := coord{r_max / 2, c_max / 2}
	// Facing direction.
	dir := dirn(0)
	// Number of nodes explicitly infected.
	infected_count := 0
	// Number of bursts to run for.
	var nburst int
	if part1 {
		nburst = 10000
	} else {
		nburst = 10000000
	}
	// Run bursts.
	for burst := 0; burst < nburst; burst++ {
		// Action depends on current node state.
		switch nodes[loc] {
		case S_INFECTED:
			dir = rightTurn(dir)
			if part1 {
				nodes[loc] = S_CLEAN
			} else {
				nodes[loc] = S_FLAGGED
			}
		case S_CLEAN:
			dir = leftTurn(dir)
			if part1 {
				nodes[loc] = S_INFECTED
				infected_count++
			} else {
				nodes[loc] = S_WEAKENED
			}
		case S_WEAKENED:
			if part1 {
				panic("weakened in part 2")
			}
			nodes[loc] = S_INFECTED
			infected_count++
		case S_FLAGGED:
			if part1 {
				panic("flagged in part 2")
			}
			dir = reverseTurn(dir)
			nodes[loc] = S_CLEAN
		}
		// Move to next node.
		move(&loc, dir)
	}
	// Show answer.
	fmt.Println(infected_count)
}
