# Advent of Code 2017: Day 15
Copyright (c) 2017 Bart Massey

Straightforward MCG things. OK. Was easy. Cleaned up a bit
by declaring generator objects. May need them later anyhow.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
