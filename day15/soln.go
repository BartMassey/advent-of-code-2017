// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 15.

package main

import (
	"aoc"
	"fmt"
	"regexp"
)

// Multiplicative Congruential PRNG state.
type MCG struct {
	state, multiplier, modulus uint64
}

// Advance the MCG by a step and return the new state.
func (mcg *MCG) advance() uint64 {
	mcg.state = (mcg.state * mcg.multiplier) % mcg.modulus
	return mcg.state
}

// Form of the input.
var rowRE *regexp.Regexp =
	regexp.MustCompile(`^Generator [A-Z] starts with ([0-9]+)$`)

// Solve the problems.
func main() {
	part1 := aoc.Setup()
	// A bunch of constants that shouldn't be hardcoded.
	// Modulus for MCGs.
	var modulus uint64 = 2147483647
	// Multipliers for MCGs.
	multipliers := []uint64 {16807, 48271}
	// Part 2 acceptance mask for MCGs.
	criteria := []uint64 {0x3, 0x7}
	// Number of pairs to process.
	var npairs int
	if part1 {
		npairs = 40000000
	} else {
		npairs = 5000000
	}
	// The generators.
	mcgs := []*MCG{}
	// Number of generators found.
	for aoc.ScanLine() {
		// Get and parse next generator description.
		line := aoc.NextLine()
		pats := rowRE.FindStringSubmatch(line)
		if pats == nil {
			panic("syntax error")
		}
		// Build and save the generator.
		state := uint64(aoc.Atoi(pats[1]))
		m := multipliers[len(mcgs)]
		mcgs = append(mcgs, &MCG{state, m, modulus})
	}
	if len(mcgs) != 2 {
		panic("unexpected number of generators")
	}
	// Current generator outputs.
	var rands [2]uint64
	// Count of matched pairs.
	count := 0
	for i := 0; i < npairs; i++ {
		// Run the generators.
		for j, v := range mcgs {
			// Keep going until a candidate
			// has been produced.
			for {
				// Advance the generator.
				rands[j] = v.advance()
				// Any candidate will do for Part 1.
				if part1 {
					break
				}
				// Only matching candidates
				// will do for Part 2.
				if rands[j] & criteria[j] == 0 {
					break
				}
			}
		}
		// Check final match.
		if (rands[0] & 0xffff) == (rands[1] & 0xffff) {
			count++
		}
	}
	fmt.Println(count)
}
