# Advent of Code 2017: Day 10
Copyright (c) 2017 Bart Massey

Straightforward programming, but really fiddly
specification. As intended.

*Note:* Code for this problem was later abstracted into a
library.

Part 1 took an unreasonable amount of debugging. Felt really
relieved to find that Part 2 worked on the first try, as it
seemed pretty undebuggable.

For the Part 1 test case, I added an extra argument to set
the ring size.  Set `GOPATH` as per top-level README and run
with

    go run soln.go 1 5 <test1.txt

I added a shell script to process the Part 2 test cases.
The outputs are to be checked manually. Set `GOPATH` as per
top-level README and run with

    sh run-tests.sh

To get the answers, set `GOPATH` as per top-level README and
run with

    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
