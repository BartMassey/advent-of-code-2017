#!/bin/sh
# Copyright © 2017 Bart Massey
# This program is licensed under the "MIT License".
# Please see the file COPYING in this distribution
# for license terms.

# Run tests for Part 2 of current day.

while read TEST
do
    echo "$TEST" | go run soln.go 2
done <tests2.txt
