// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 10.

package main

import (
	"aoc"
	"aoc/knothash"
	"fmt"
	"os"
	"strings"
)

// Read the inputs and compute the answers.
func main() {
	// Standard setup.
	part1 := aoc.Setup()
	// Default ring size.
	ring_size := 256
	// If specified, set the ring size.
	if len(os.Args) > 2 {
		ring_size = aoc.Atoi(os.Args[2])
	}
	// Read the lengths.
	lengths_str := aoc.ReadLine()
	if part1 {
		// Treat the input as a comma-separated list
		// of ints.
		lengths_list := strings.Split(lengths_str, ",")
		// Convert each length to a character and
		// stick it on the end of the key.
		var key string
		for _, v := range lengths_list {
			length := aoc.Atoi(v)
			key += string(rune(length))
		}
		// Print the product of the first two
		// elements of the ring.
		ring := knothash.ShuffleRing(ring_size, 1, false, key)
		fmt.Println(ring[0] * ring[1])
	} else {
		// Work on 16-element blocks.
		hash := knothash.KnotHash(lengths_str)
		// Show the digits.
		for _, byte := range hash {
			fmt.Printf("%02x", byte)
		}
		// End the line.
		fmt.Println()
	}
}
