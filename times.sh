#!/bin/sh
for D in day??
do
    echo $D
    ( cd $D
      go clean
      if [ -f .build ]
      then
          sh .build 2>&1
      else
          go build soln.go 2>&1
      fi
      if [ $D = day25 ]
      then
          echo -n "sole part: "
          ( /usr/bin/time -f '%e' sh -c "./soln <input.txt" ) 2>&1
          go clean
          continue
      fi
      for PART in 1 2
      do
          echo -n "part $PART: "
          egrep " go run soln.go $PART " README.md |
          sed 's=go run soln.go=./soln=' |
          ( read CMD; /usr/bin/time -f '%e' sh -c "$CMD" ) 2>&1 >/dev/null
      done
      go clean )
done
