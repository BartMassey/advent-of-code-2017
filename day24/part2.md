# Day 24: Part Two

The bridge you've built isn't long enough; you can't [jump the rest of
the way]{title="Who do you think you are, Mario?"}.

In the example above, there are two longest bridges:

-   `0/2`--`2/2`--`2/3`--`3/4`
-   `0/2`--`2/2`--`2/3`--`3/5`

Of them, the one which uses the `3/5` component is stronger; its
strength is `0+2 + 2+2 + 2+3 + 3+5 = 19`.

*What is the strength of the longest bridge you can make?* If you can
make multiple bridges of the longest length, pick the *strongest* one.

Your puzzle answer was `1824`.
