# Advent of Code 2017: Day 24
Copyright (c) 2017 Bart Massey

OK, a straightforward depth-first search. I was surprised
that naïve search was fast enough, but it seemed to run
basically instantaneously on all parts. I used separate
marking, an over-fancy datatype, and a side map to keep
track of which pieces had which ports.

Go made it harder than expected by being really confusing
about when and how maps and slices get initialized. The fact
that nil slices can be appended to but nil maps can't be
inserted in is a bit counterintuitive.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
