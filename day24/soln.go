// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 24.

package main

import (
	"aoc"
	"fmt"
	"strings"
)

// Port count.
type port int

// A single pice.
type piece struct {
	// Mostly-redundant position in piece list.
	id int
	// Port count of ends in arbitrary order.
	ends [2]port
}

// A bunch of global variables because too many parameters
// is silly and they are all just passed by reference
// anyway.

// A port locator maps each port count to a set of piece
// ids that have that port count for at least one end.
type portLoc map[port]map[int]bool
var loc portLoc

// Looking for strongest bridge (vs longest/strongest
// bridge).
var strongest bool

// Pieces to build the bridge out of.
var pieces []piece

// Which pieces are currently in use.
var mark []bool

// Pick a starting piece for the bridge that matches target,
// then try to place more pieces. Returns the strength and
// length of the "best" bridge found according to the
// criteria. Returns strength -1 on failure.
func buildBridge(target port) (int, int) {
	// Strongest bridge that can be built.
	strength := -1
	// Longest bridge that can be built.
	length := 0
	// Try at all the candidate pieces that match
	// target.
	for i := range loc[target] {
		// Dig out the piece.
		p := pieces[i]
		if mark[i] {
			// Already in use.
			continue
		}
		// We will use it for now.
		mark[i] = true
		// Find the matching end and reorient the
		// piece.
		if p.ends[0] != target {
			if p.ends[1] != target {
				fmt.Println(target, p)
				panic("internal error: wrong piece")
			}
			p.ends[0], p.ends[1] = p.ends[1], p.ends[0]
		}
		// Strength of bridge. Starts with this
		// piece.
		s0 := int(p.ends[0]) + int(p.ends[1])
		// Length of bridge. Starts with this single
		// piece. "Journey of 1000 miles" blah blah.
		l0 := 1
		// Try to build more bridge.
		s, l := buildBridge(p.ends[1])
		// If there was more bridge, record its
		// length and value.
		if s >= 0 {
			s0 += s
			l0 += l
		}
		// Update our strength and length according
		// to the criteria.
		if ((strongest || l0 == length) && s0 > strength) ||
			(!strongest && l0 > length) {
			strength = s0
			length = l0
		}
		// Done with this piece now.
		mark[p.id] = false
	}
	// What's the best we could do?
	return strength, length
}

// Solve the problem.
func main() {
	// Part 1 is strongest, Part 2 is longest then
	// strongest.
	strongest = aoc.Setup()
	// Read the instance description.
	id := 0
	for aoc.ScanLine() {
		desc := aoc.NextLine()
		end_descs := strings.Split(desc, "/")
		p := piece {
			id,
			[2]port {
				port(aoc.Atoi(end_descs[0])),
				port(aoc.Atoi(end_descs[1])),
			},
		}
		id++
		pieces = append(pieces, p)
	}
	aoc.LineErr()
	// Build the port locator.
	loc = make(portLoc)
	for i, v := range pieces {
		for j := 0; j < 2; j++ {
			if loc[v.ends[j]] == nil {
				// Need to create the missing map.
				loc[v.ends[j]] = make(map[int]bool)
			}
			loc[v.ends[j]][i] = true
		}
	}
	// Build the mark table.
	mark = make([]bool, len(pieces))
	// Run the search.
	strength, _ := buildBridge(0)
	// Report the result.
	fmt.Println(strength)
}
