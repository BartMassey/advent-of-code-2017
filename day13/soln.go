// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 13.

package main

import (
	"aoc"
	"fmt"
	"strings"
)

// Scanner representation.
type Scan struct {
	depth int
	srange int
}

// Read in all the scanners.
func readInput() []Scan {
	var scans []Scan
	for aoc.ScanLine() {
		line := aoc.NextLine()
		fields := strings.Split(line, ": ")
		depth := aoc.Atoi(fields[0])
		srange := aoc.Atoi(fields[1])
		scans = append(scans, Scan{depth, srange})
	}
	aoc.LineErr()
	return scans
}

// Solve Part 1, returning the trip severity.
func soln1(scans []Scan) int {
	trip_severity := 0
	for _, s := range scans {
		// A scanner is cyclic: this expression
		// catches the case where it hits.
		if s.depth % (2 * (s.srange - 1)) == 0 {
			trip_severity += s.depth * s.srange
		}
	}
	return trip_severity
}

// Solve Part 2, returning the minimum delay.
func soln2(scans []Scan) int {
	// See if a scanner with delay d0 misses.
	try_depth := func(d0 int) bool {
		// XXX This code is a repetitive copy-paste
		// of soln1 code, but I don't see an easy
		// way to clean it up offhand.
		for _, s := range scans {
			if (d0 + s.depth) % (2 * (s.srange - 1)) == 0 {
				return false
			}
		}
		return true
	}

	// Try each delay d0 until one gets through.
	d0 := 0
	for {
		if try_depth(d0) {
			return d0
		}
		d0++
	}
}

// Solve the specified problem.
func main() {
	part1 := aoc.Setup()
	scans := readInput()
	if part1 {
		trip_severity := soln1(scans)
		fmt.Println(trip_severity)
	} else {
		delay := soln2(scans)
		fmt.Println(delay)
	}
}
