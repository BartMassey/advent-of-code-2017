# Advent of Code 2017: Day 13
Copyright (c) 2017 Bart Massey

Hooray modulo arithmetic! Part 1 can be solved with some
simple modular equality tests, yielding a nice linear-time
solution.

There's probably some LCM-based solution for Part 2 that is
also linear-time. I thought about it for a few minutes and
couldn't see it, so I just brute-forced it for an
exponential-time solution. It's gross, but runs in less than
a half-second, so meh.

Set `GOPATH` as per top-level README and run with
    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
