# Advent of Code 2017: Day 25
Copyright (c) 2017 Bart Massey

Sigh. My insistence on parsing the input inside the program
led to a couple of hours of dinking around. My input reading
library in `aoc` is not too clean, and this is where it bit
me. I eventually kludged something together and got the
parse going. From there it was a short step to actually
running the Turing Machine and doing the counting.

Set `GOPATH` as per top-level README and run with
    go run soln.go <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
