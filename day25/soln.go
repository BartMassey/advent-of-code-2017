// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 25.

package main

import (
	"aoc"
	"aoc/bitrow"
	"fmt"
	"regexp"
)

// True to show text during input read.
const traceRead = false

// Type of bits on tape and in machine.
type bit bool

// Moves left and right.
type dirn int
const DIRN_LEFT = dirn(-1)
const DIRN_RIGHT = dirn(1)

// State transition action.
type trans struct {
	// What to write on the tape.
	write bit
	// Which way to move the head.
	move dirn
	// What state to enter next.
	nexts rune
}

// Pair of state transitions; one for false (0), one for
// true (1).
type trans2 [2]trans

// Turing Machine program.
type prog struct {
	// Start state.
	start rune
	// Stop after this many steps.
	steps int
	// Instructions in state machine.
	insns map[rune]trans2
}

// State of a computation.
type state struct {
	// Current state.
	curs rune
	// Tape position.
	posn int
	// Tape contents.
	tape bitrow.BitRow
}

// Read and parse the input into a program.
func readProg() *prog {
	// Regexes for various input lines.
	xstart := regexp.MustCompile(`^Begin in state ([A-Z])\.$`)
	xsteps := regexp.MustCompile(`^Perform a diagnostic checksum after ([0-9]+) steps\.$`)
	xlabel := regexp.MustCompile(`^In state ([A-Z]):$`)
	xread  := regexp.MustCompile(`^  If the current value is ([01]):$`)
	xwrite := regexp.MustCompile(`^    - Write the value ([01])\.$`)
	xmove  := regexp.MustCompile(`^    - Move one slot to the (left|right)\.$`)
	xnext  := regexp.MustCompile(`^    - Continue with state ([A-Z])\.$`)

	// Functions to read lines with various kinds
	// operands in them. Assumption is one operand per
	// line.

	// Read a line containing a special string.
	read_string := func(re *regexp.Regexp) string {
		line := aoc.ReadLine()
		if traceRead {
			fmt.Println(line)
		}
		matches := re.FindStringSubmatch(line)
		if len(matches) != 2 {
			if traceRead {
				fmt.Println(re.String())
				fmt.Println(matches)
			}
			panic("bad match")
		}
		return matches[1]
	}

	// Read a line containing a state descriptor.
	read_state := func(re *regexp.Regexp) rune {
		st := read_string(re)
		chs := []rune(st)
		if len(chs) != 1 {
			panic("internal error: bad state")
		}
		return chs[0]
	}	

	// Read a line containing an int.
	read_num := func(re *regexp.Regexp) int {
		ns := read_string(re)
		return aoc.Atoi(ns)
	}	

	// Read a line containing a bit.
	read_bit := func(re *regexp.Regexp) bit {
		n := read_num(re)
		switch n {
		case 0:
			return bit(false)
		case 1:
			return bit(true)
		}
		panic("internal error: bad bit")
	}	

	// Set up the return data structure.
	var p prog
	// Load preamble.
	p.start = read_state(xstart)
	p.steps = read_num(xsteps)
	// Set up and fill state transition map.
	p.insns = make(map[rune]trans2)
	// XXX We check availability of just the first line
	// of each block.
	for aoc.ScanLine() {
		// Should start with a blank line.
		blank_line := aoc.NextLine()
		if blank_line != "" {
			panic("expected blank line")
		}
		// Read in the block description.
		stl := read_state(xlabel)
		var bs trans2
		for i := 0; i < 2; i++ {
			source := read_bit(xread)
			if source != bit(i != 0) {
				panic("bad source")
			}
			bs[i].write = read_bit(xwrite)
			var di dirn
			switch read_string(xmove) {
			case "left":
				di = DIRN_LEFT
			case "right":
				di = DIRN_RIGHT
			default:
				panic("internal error: bad direction")
			}
			bs[i].move = di
			bs[i].nexts = read_state(xnext)
		}
		p.insns[stl] = bs
	}
	aoc.LineErr()
	return &p
}

// Convert a bit to an int.
func (b bit) intVal() int {
	switch bool(b) {
	case false:
		return 0
	case true:
		return 1
	}
	panic("internal error: bit neither true nor false")
}

// Run the given program in the given state.
func runProg(p *prog, s *state) {
	// Number of steps is fixed.
	for i := 0; i < p.steps; i++ {
		// Read tape.
		b := bit(s.tape.Get(s.posn))
		// Copy transition based on state.
		tr := p.insns[s.curs][b.intVal()]
		// Do the transition actions.
		s.tape.Set(s.posn, bool(tr.write))
		s.posn += int(tr.move)
		s.curs = tr.nexts
	}
}

// Return the number of one (true) bits on the tape.
func countOnes(tape bitrow.BitRow) int {
	tapemap := tape.ToMap()
	count := 0
	// Look at previously-stored keys.
	for _, v := range tapemap {
		// Check for one bit.
		if v {
			count++
		}
	}
	return count
}

// Solve the problems.
func main() {
	aoc.Setup()
	p := readProg()
	var s state
	s.curs = p.start
	s.tape = bitrow.New()
	runProg(p, &s)
	fmt.Println(countOnes(s.tape))
}
