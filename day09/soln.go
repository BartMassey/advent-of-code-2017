// Copyright © 2017 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file COPYING in this distribution
// for license terms.

// Advent of Code Day 9.

package main

import (
	"aoc"
	"fmt"
)

// Type of states of state machine.
type State uint

// States of state machine.
const (
      STATE_STREAM State = iota
      STATE_GARBAGE
      STATE_ESCAPE
)

// Given a string representing a stream, return
// the score of the brackets and the count of the
// garbage for that string.
func scoreStream(stream string) (int, int) {
	// Nesting depth in stream.
	depth := 0
	// Stream score.
	score := 0
	// Number of garbage characters seen.
	garbage := 0
	// State of stream.
	state := STATE_STREAM
	// Iterate over Unicode code points ("runes") of stream.
	for _, r := range stream {
		// State-dependent action.
		switch state {
		case STATE_ESCAPE:
			// Finish processing the escape.
			state = STATE_GARBAGE
		case STATE_GARBAGE:
			switch r {
			case '!':
				// Start processing the escape.
				state = STATE_ESCAPE
			case '>':
				// Finish processing the garbage.
				state = STATE_STREAM
			default:
				// Count the garbage character.
				garbage++
			}
		case STATE_STREAM:
			switch r {
			case '{':
				// Enter a new bracket depth.
				depth++
				// Score on entry. (Doesn't matter.)
				score += depth
			case '}':
				depth--
				// Leave current bracket depth.
				if depth < 0 {
					// Input with extra right
					// brackets is malformed.
					panic("underdepth")
				}
			case '<':
				// Start processing garbage.
				state = STATE_GARBAGE
			case ',':
				// Commas have no semantic
				// meaning, so skip them.
			default:
				// Stream is specified to
				// have only certain
				// characters.
				panic("bad char in stream")
			}
		default:
			panic("internal error: bad state")
		}
	}
	// Expect to finish in some sensible situation.
	if state != STATE_STREAM {
		panic("surprising end state")
	}
	if depth != 0 {
		panic("unclosed brackets")
	}
	// Return answers to both parts.
	return score, garbage
}


// Set up processing, run the problem, print the answer.
func main() {
	part1 := aoc.Setup()
	line := aoc.ReadLine()
	score, garbage := scoreStream(line)
	if part1 {
		fmt.Println(score)
	} else {
		fmt.Println(garbage)
	}
}
