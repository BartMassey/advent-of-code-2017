#!/bin/sh
# Copyright © 2017 Bart Massey
# This program is licensed under the "MIT License".
# Please see the file COPYING in this distribution
# for license terms.

# Run tests for current day.

PART=$1
TESTS=tests${PART}.txt
tr ' ' '_' <$TESTS |
sed 's/_\([0-9]*\)$/ \1/' |
while read TEST EXPECTED
do
    GOT=`echo "$TEST" | go run soln.go $PART`
    echo "$TEST" $EXPECTED $GOT
done
