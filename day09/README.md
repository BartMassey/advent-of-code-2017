# Advent of Code 2017: Day 9
Copyright (c) 2017 Bart Massey

State machine! Fortunately, right before the problem hit I'd
been reading about how to do enums in Go. It's not bad,
actually. So I had a nice representation for states.

The code here is pretty straightforward.

I added a shell script to process the test cases because
there were so many. Set `GOPATH` as per top-level README and
run with

    sh run-tests.sh 1
    sh run-tests.sh 2

The scores on the right in the output should match in both
cases.

To get the answers, set `GOPATH` as per top-level README and
run with

    go run soln.go 1 <input.txt
    go run soln.go 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file COPYING in this distribution
for license terms.
